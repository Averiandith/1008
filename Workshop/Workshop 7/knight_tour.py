from Task_1 import Tour

def print_menu(Tour):
    print("1. Start")

    last_ctr = 2
    if Tour.current_pos is not None:
        last_ctr = len(possible_moves) + 2
        possible_moves = Tour.next_moves()
        for i in range(len(possible_moves)):
            print(str(i+2) + ". " + possible_moves[i])
    print(str(last_ctr) + ". End")

    instruction = int(input("What's your instruction? "))

    if instruction == 1:
        Tour.create_board()

        start_col_input = int(input("Please enter your desired col: "))
        start_row_input = int(input("Please enter your desired row: "))

        position_list = [start_col_input, start_row_input]

        col_out_of_bound = start_col_input >= 8 or start_col_input < 0
        row_out_of_bound = start_row_input >= 8 or start_row_input < 0
        if col_out_of_bound or row_out_of_bound:
            raise Exception("Position out of range")

        Tour.set_current_pos([start_col_input, start_row_input])
        Tour.place_knight(position_list)

    #if instruction ==  Last item in the list, break loop


KnightTouring = Tour()
print_menu(KnightTouring)
KnightTouring.print_board()


