class Tour:
    def __init__(self):
        self.size = 8
        self.create_board()
        self.current_pos = None

    def create_board(self):
        chess_board = []
        for i in range(self.size):
            chess_board.append(self.size * [""])
        self.chess_board = chess_board

    def next_moves(self):
        i = self.current_pos[0]
        j = self.current_pos[1]
        possible_list = []

        move_by_knights = [[1, -2], [1, 2], [-1, -2], [-1, 2], [2, -1], [-2, 1], [2, 1], [-2, -1]]

        for i in range(len(move_by_knights)):
            x = move_by_knights[i][0]
            y = move_by_knights[i][1]

            if (i + x) <= length and (i + x) >= 0:
                if (j + y) <= length and (j + y) >= 0:
                    if chess_board[i + x][j + y] != "*":
                        inner_list = [(i + x), (j + y)]
                        possible_list.append(inner_list)

        return possible_list

    def set_current_pos(self, position_list):
        self.current_pos = position_list

    def print_board(self):
        for i in range(len(self.chess_board)):
            print(self.chess_board[i])

    def place_knight(self, position_list):
        x = position_list[0]
        y = position_list[1]

        self.chess_board[x][y] = "K"





