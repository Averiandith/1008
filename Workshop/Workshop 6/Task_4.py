def swap(aList, i, j):
    """
    Standard swap function
    :param aList:
    :param i:
    :param j:
    :return:
    """
    aList[i], aList[j] = aList[j], aList[i]

def insertionSort(aList):
    """
    Function for insertion sort
    :param aList:
    :return:
    """
    n = len(aList)
    for k in range(1, n):
        j = k
        while aList[j - 1] > aList[j] and j > 0:
            swap(aList, j-1, j)
            j -= 1

def getMinIndex(myList, start, stop):
    """
    Function to obtain min index in a list
    :param myList:
    :param start:
    :param stop:
    :return:
    """
    minIndex = start
    for i in range(start, stop):
        if myList[i] < myList[minIndex]:
            minIndex = i
    return minIndex

def selectionSort(aList):
    """
    Function to sort a list using selection sort
    :param aList:
    :return:
    """
    n = len(aList)
    for index in range(n):
        minPosition = getMinIndex(aList, index, n)
        swap(aList, index, minPosition)

def shakerSort(aList):
    """
    Function to sort a list using shaker sort
    :param aList:
    :return:
    """
    noSwaps = True
    n = len(aList)
    movingRightStart = 0
    movingRightEnd = n - 1
    movingLeftStart = n - 1
    movingLeftEnd = 0

    for j in range(n - 1):
        if (j % 2 == 0):
            for i in range(movingRightStart, movingRightEnd):
                if aList[i] > aList[i+1]:
                    noSwaps = False
                    swap(aList, i, i+1)
            movingLeftStart -= 1
            movingRightEnd -= 1

        else:
            for i in range(movingLeftStart, movingLeftEnd, -1):
                if aList[i-1] > aList[i]:
                    noSwaps = False
                    swap(aList, i, i-1)
            movingLeftEnd += 1
            movingRightStart += 1

