import timeit
import random


def swap(aList, i, j):
    """
    Just a simple swap function
    :param aList:
    :param i:
    :param j:
    :return:
    """
    aList[i], aList[j] = aList[j], aList[i]

def shakerSort(aList):
    noSwaps = True
    n = len(aList)
    movingRightStart = 0
    movingRightEnd = n - 1
    movingLeftStart = n - 1
    movingLeftEnd = 0

    for j in range(n - 1):
        if (j % 2 == 0):
            for i in range(movingRightStart, movingRightEnd):
                if aList[i] > aList[i+1]:
                    noSwaps = False
                    swap(aList, i, i+1)
            movingLeftStart -= 1
            movingRightEnd -= 1

        else:
            for i in range(movingLeftStart, movingLeftEnd, -1):
                if aList[i-1] > aList[i]:
                    noSwaps = False
                    swap(aList, i, i-1)
            movingLeftEnd += 1
            movingRightStart += 1


def timeShakerSort(aList):
    """
    Time taken to sort using shaker sort
    :param aList:
    :return:
    """
    start = timeit.default_timer()
    shakerSort(aList)
    taken = (timeit.default_timer() - start)
    return taken

def tableTimeShakerSort():
    """
    Tabulate time taken for shaker sort
    :return: print iterations and time taken
    """
    n = 2
    aList = []
    nList = []
    timeList = []
    while True:
        for i in range(n):
            aList.append(random.random())
        nList.append(n)
        timeList.append(timeShakerSort(aList))
        n *= 2
        if n > 1024:
            break
    for i in nList:
        print(i)
    for j in timeList:
        print(j)

tableTimeShakerSort()

def tableAvgTimeShakerSort():
    """
    By implementing a fixed amount of list range 100 each time, then tabulate the time taken
    :return: print current iteration and lists of time
    """
    n = 2
    aList = []
    nList = []
    timeList = []
    for i in range(100):
        for j in range(n):
            aList.append(random.random())
        nList.append(n)
        timeList.append(timeShakerSort(aList))
        n *= 2
        if n > 1024:
            break
    for i in nList:
        print(i)
    for j in timeList:
        print(j)

tableAvgTimeShakerSort()