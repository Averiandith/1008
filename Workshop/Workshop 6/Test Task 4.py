from Task_4 import *
import random

def test_sorting():
    """
    I import my sorting algorithm inside here, and test whether the algorithm works.
    :exception List is not sorted:
    :return: All test passed: Function will print all tests passed, if lists are sorted.
    """
    test_case_1 = [5, 4, 3, 2, 1]
    insertionSort(test_case_1)
    assert test_case_1 == [1, 2, 3, 4, 5], "List is not sorted"

    test_case_1 = [5, 4, 3, 2, 1]
    selectionSort(test_case_1)
    assert test_case_1 == [1, 2, 3, 4, 5], "List is not sorted"

    test_case_1 = [5, 4, 3, 2, 1]
    shakerSort(test_case_1)
    assert test_case_1 == [1, 2, 3, 4, 5], "List is not sorted"

    print("All tests passed.")

test_sorting()