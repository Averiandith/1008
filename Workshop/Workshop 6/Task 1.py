# Task 1
def sumItems(aList):
    """
    It is a sum function that sums up all item in a list
    :param aList: aList of your choice that only accepts number
    :return: return the total count
    """
    ctr = 0
    for item in aList:
        ctr += item
    return ctr

# Best Case: O(N) Still have to loop through the whole list even though no item is present (doesn't depend on the content of the list)
# Worst Case: O(N)

