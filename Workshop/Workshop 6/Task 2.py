# Task 2

import timeit
import random

def timeSumItems(aList):
    """
    Time function to count time that it takes to sum items
    :param aList: aList of your choice
    :return: return time taken
    """
    start = timeit.default_timer()
    ctr = 0
    for item in aList:
        ctr += item
    taken = (timeit.default_timer() - start)
    return taken

def tableTimeSumItems():
    """
    A function to tabulate time taken to sum items in lists
    :return: print current iteration and time taken
    """
    n = 2
    aList = []
    sums = []
    ns = []
    while True:
        for i in range(n):
            aList.append(random.random())
        ns.append(n)
        sums.append(timeSumItems(aList))
        n = n * 2
        if n > 1024:
            break
    for i in sums:
        print(i)
    for j in ns:
        print(j)

tableTimeSumItems()
