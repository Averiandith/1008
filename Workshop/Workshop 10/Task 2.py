import numpy as np


class Item:
    def __init__(self, value=0, weight=0):
        assert type(weight) == int
        self.value = value
        self.weight = weight

    def __str__(self):
        return "(v={}: w={})".format(self.value, self.weight)

    def __repr__(self):
        return str(self)


a_list = []


def knapsack(list_of_items, capacity):
    assert len(list_of_items), "No items here"
    assert capacity > 0, "Did you bring your knapsack?"
    assert type(capacity) == int
    global a_list

    table = np.zeros(shape=(len(list_of_items)+1, capacity+1))

    for i, item in enumerate(list_of_items, start = 1):
        for j in range(1, capacity + 1):
            if item.weight > j:
                table[i, j] = table[i - 1, j]
            else:
                table[i, j] = max(table[i - 1, j], item.value + table[i - 1, j - item.weight])

    j = capacity
    i = len(list_of_items)
    while j > 0:
        while i > 0:
            if table[i - 1, j] != table[i, j]:          # Follow slides, follow the last cell btm right
                a_list.append(i)
                j = j - list_of_items[i - 1].weight
                break
            i -= 1

    return table[-1, -1]


