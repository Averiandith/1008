# Dynamic Programming Approach


def fibonacci(n):
    sequence = [0] * n
    sequence[0] = 1
    sequence[1] = 1
    for i in range(2, n):
        sequence[i] = sequence[i - 1] + sequence[i - 2]
    return sequence


print(fibonacci(6))
sequence = fibonacci(6)


for i in range(sequence[0], sequence[-1] + 1):
    ctr = 0
    for item in sequence:
        if item == i:
            ctr += 1
    print("Fibonacci({}) has appeared {} times".format(i, ctr))





