def fibonacci(n, a_list):
    a_list[n - 1] += 1
    if n == 1 or n == 2:
        return 1
    else:
        return fibonacci(n - 1, a_list) + fibonacci(n - 2, a_list)


def num_of_elements(n):
    elements = [0] * n
    fibonacci(n, elements)
    return elements


fib_elements = num_of_elements(5)
print(fib_elements)
for i in range(len(fib_elements)):
    print("Fibonacci({}) has appeared {} times".format(i + 1, fib_elements[i]))

