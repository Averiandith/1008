"""
author: Yan Choong Tan
since: 25/7/2018
modified: 26/7/2018
"""
def printMenu():
    """
    This function just prints the menu whenever called.
    """
    print("\nMenu: ")
    print("1. append ")
    print("2. sort ")
    print("3. print ")
    print("4. clear ")
    print("5. reverse ")
    print("6. pop")
    print("7. insert")
    print("8. quit ")


myList = []
quit = False
inputLine = None

while not quit:
    printMenu()

    command = int(input("\nEnter command: "))

    if command == 1:
        item = int(input("Item? "))
        myList.append(item)
    elif command == 2:
        myList.sort()
    elif command == 3:
        print(myList)
    elif command == 4:
        myList.clear()
    elif command == 5:
        myList.reverse()
    elif command == 6:
        print(myList.pop())
    elif command == 7:
        x = int(input("Which position do u wish to insert? "))
        y = int(input("What is your item? "))
        myList.insert(x, y)
    elif command == 8:
        quit = True

