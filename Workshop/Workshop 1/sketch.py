def decimalToHex(decimalNumber):
    myList = []
    while decimalNumber > 0:
        rem = decimalNumber % 16
        if rem > 9:
            if rem == 10:
                rem = 'A'
            elif rem == 11:
                rem = 'B'
            elif rem == 12:
                rem = 'C'
            elif rem == 13:
                rem = 'D'
            elif rem == 14:
                rem = 'E'
            else:
                rem = 'F'
        myList.insert(0, str(rem))
        decimalNumber = decimalNumber // 16
    return '0x' + ''.join(myList)

# Binary To Decimal
binaryNumber = 1100011101
myList = list(str(binaryNumber))
myList.reverse()
length = len(myList)
for i in range(length):
    myList[i] = int(myList[i])
decimalNumber = 0
for index in range(length):
    decimalNumber += (myList[index]) * (2**index)
print(decimalNumber)

def binaryToDecimal(binaryNumber):
    myList = list(str(binaryNumber))
    myList = myList[::-1]
    length = len(myList)
    for i in range(length):
        myList[i] = int(myList[i])
    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (2**index)
    return "0x" + str(decimalNumber)

print(binaryToDecimal(1100011101))

# Hexadecimal To Decimal
Hexadecimal = "123C9F"
myList = list(Hexadecimal)
myList.reverse()
length = len(myList)

for i in range(length): #A10 B11 C12 D13 E14 F15
    if myList[i] == "A":
        myList[i] = 10
    elif myList[i] == "B":
        myList[i] = 11
    elif myList[i] == "C":
        myList[i] = 12
    elif myList[i] == "D":
        myList[i] = 13
    elif myList[i] == "E":
        myList[i] = 14
    elif myList[i] == "F":
        myList[i] = 15

for j in range(length):
    myList[j] = int(myList[j])

decimalNumber = 0
for index in range(length):
    decimalNumber += (myList[index]) * (16**index)
print(decimalNumber)

def hexadecimalToDecimal(hexadecimal):
    myList = list(hexadecimal)
    myList = myList[::-1]
    length = len(myList)

    for i in range(length):  # A10 B11 C12 D13 E14 F15
        if myList[i] == "A":
            myList[i] = 10
        elif myList[i] == "B":
            myList[i] = 11
        elif myList[i] == "C":
            myList[i] = 12
        elif myList[i] == "D":
            myList[i] = 13
        elif myList[i] == "E":
            myList[i] = 14
        elif myList[i] == "F":
            myList[i] = 15

    for j in range(length):
        myList[j] = int(myList[j])

    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (16 ** index)
    return decimalNumber

print(hexadecimalToDecimal("123C9F"))
# Hexadecimal To Binary
Hexadecimal = "AFC934B2D"
myList = list(Hexadecimal)
length = len(myList)

for i in range(length): #A10 B11 C12 D13 E14 F15 "0x"Hex "0b"Bin
    if myList[i] == "A":
        myList[i] = 10
    elif myList[i] == "B":
        myList[i] = 11
    elif myList[i] == "C":
        myList[i] = 12
    elif myList[i] == "D":
        myList[i] = 13
    elif myList[i] == "E":
        myList[i] = 14
    elif myList[i] == "F":
        myList[i] = 15

for j in range(length):
    myList[j] = int(myList[j])

binaryNumber = ""

for k in range(length):
    if myList[k] == 0:
        binaryNumber += "0000"
    elif myList[k] == 1:
        binaryNumber += "0001"
    elif myList[k] == 2:
        binaryNumber += "0010"
    elif myList[k] == 3:
        binaryNumber += "0011"
    elif myList[k] == 4:
        binaryNumber += "0100"
    elif myList[k] == 5:
        binaryNumber += "0101"
    elif myList[k] == 6:
        binaryNumber += "0110"
    elif myList[k] == 7:
        binaryNumber += "0111"
    elif myList[k] == 8:
        binaryNumber += "1000"
    elif myList[k] == 9:
        binaryNumber += "1001"
    elif myList[k] == 10:
        binaryNumber += "1010"
    elif myList[k] == 11:
        binaryNumber += "1011"
    elif myList[k] == 12:
        binaryNumber += "1100"
    elif myList[k] == 12:
        binaryNumber += "1100"
    elif myList[k] == 13:
        binaryNumber += "1101"
    elif myList[k] == 14:
        binaryNumber += "1110"
    elif myList[k] == 15:
        binaryNumber += "1111"

print(binaryNumber)

def hexadecimalToBinary(hexadecimal):
    myList = list(Hexadecimal)
    length = len(myList)

    for i in range(length):
        if myList[i] == "A":
            myList[i] = 10
        elif myList[i] == "B":
            myList[i] = 11
        elif myList[i] == "C":
            myList[i] = 12
        elif myList[i] == "D":
            myList[i] = 13
        elif myList[i] == "E":
            myList[i] = 14
        elif myList[i] == "F":
            myList[i] = 15

    for j in range(length):
        myList[j] = int(myList[j])

    binaryNumber = ""

    for k in range(length):
        if myList[k] == 0:
            binaryNumber += "0000"
        elif myList[k] == 1:
            binaryNumber += "0001"
        elif myList[k] == 2:
            binaryNumber += "0010"
        elif myList[k] == 3:
            binaryNumber += "0011"
        elif myList[k] == 4:
            binaryNumber += "0100"
        elif myList[k] == 5:
            binaryNumber += "0101"
        elif myList[k] == 6:
            binaryNumber += "0110"
        elif myList[k] == 7:
            binaryNumber += "0111"
        elif myList[k] == 8:
            binaryNumber += "1000"
        elif myList[k] == 9:
            binaryNumber += "1001"
        elif myList[k] == 10:
            binaryNumber += "1010"
        elif myList[k] == 11:
            binaryNumber += "1011"
        elif myList[k] == 12:
            binaryNumber += "1100"
        elif myList[k] == 12:
            binaryNumber += "1100"
        elif myList[k] == 13:
            binaryNumber += "1101"
        elif myList[k] == 14:
            binaryNumber += "1110"
        elif myList[k] == 15:
            binaryNumber += "1111"

    return "0b" + str(binaryNumber)

print(hexadecimalToBinary("AFC934B2D"))




#Binary To Hexadecimal
binaryNumber = 101011111100100100110100101100101101
myList = list(str(binaryNumber))
myList.reverse()
length = len(myList)
for i in range(length):
    myList[i] = int(myList[i])
#Binary Change To Decimal
decimalNumber = 0
for index in range(length):
    decimalNumber += (myList[index]) * (2**index)
# print(decimalNumber)
#Decimal Change To Hexadecimal

print(decimalToHex(decimalNumber))

def binaryToHexadecimal(binaryNumber):
    myList = list(str(binaryNumber))
    myList = myList[::-1]
    length = len(myList)
    for i in range(length):
        myList[i] = int(myList[i])
    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (2 ** index)
    return decimalToHex(decimalNumber)

print(binaryToHexadecimal(101011111100100100110100101100101101))

"""while True:
    for i in range(a, b):
        secList.append(myList[i])
        a += 4
        b += 4
print(myList)
print(secList)"""

