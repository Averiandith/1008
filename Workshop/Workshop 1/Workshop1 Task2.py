"""
author: Yan Choong Tan
since: 23/7/2018
modified: 26/7/2018
"""
def decimal_to_binary(decimal_number):
    """
    This is a function to convert decimal to binary.
    :param decimal_number: a decimal number
    :return: return the binary value
    """
    my_list = []
    while decimal_number > 0:
        rem = decimal_number % 2
        my_list.insert(0, str(rem))
        decimal_number = decimal_number // 2
    return '0b' + ''.join(my_list)

number = int(input('Enter a number: '))
print('The binary representation is ' + decimal_to_binary(number))


def decimal_to_hex(decimal_number):
    """
    This is a function to convert decimal number to hexadecimal
    :param decimal_number: a decimal number
    :return: return the binary value
    """
    my_list = []
    while decimal_number > 0:
        rem = decimal_number % 16
        if rem > 9:
            if rem == 10:
                rem = 'A'
            elif rem == 11:
                rem = 'B'
            elif rem == 12:
                rem = 'C'
            elif rem == 13:
                rem = 'D'
            elif rem == 14:
                rem = 'E'
            else:
                rem = 'F'
        my_list.insert(0, str(rem))
        decimal_number = decimal_number // 16
    return '0x' + ''.join(my_list)
number = int(input('Enter a number: '))
print('The hexadecimal representation is ' + decimal_to_hex(number))

# This program just accepts your name and output hello and welcome with your name between it.
name = input('Enter name (max 60 chars): ')
print('Hello ' + name + '. Welcome')

# This program takes Celsius as input and output the result in Fahrenheit.
temp_C = int(input('Enter temperature in Celsius '))
temp_F = int(9*temp_C/5 + 32)
print('Temperature in Fahrenheit is ' + str(temp_F))
