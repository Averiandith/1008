"""
author: Yan Choong Tan
since: 25/7/2018
modified: 26/7/2018
"""
def decimalToBinary(decimalNumber):
    """
    This function converts decimal to binary.
    :param decimalNumber: input decimal value.
    :return: output a binary value.
    :precondition: only accepts integer values.
    """
    myList = []
    while decimalNumber > 0:
        rem = decimalNumber % 2
        myList.insert(0, str(rem))
        decimalNumber = decimalNumber // 2
    return '0b' + ''.join(myList)

def decimalToHex(decimalNumber):
    """
    This function converts decimal to binary
    :param decimalNumber: input decimal value.
    :return: output a hexadecimal value.
    :precondition: only accepts integer values.
    """
    myList = []
    while decimalNumber > 0:
        rem = decimalNumber % 16
        if rem > 9:
            if rem == 10:
                rem = 'A'
            elif rem == 11:
                rem = 'B'
            elif rem == 12:
                rem = 'C'
            elif rem == 13:
                rem = 'D'
            elif rem == 14:
                rem = 'E'
            else:
                rem = 'F'
        myList.insert(0, str(rem))
        decimalNumber = decimalNumber // 16
    return '0x' + ''.join(myList)

def binaryToDecimal(binaryNumber):
    """
    This function converts binary to decimal.
    :param binaryNumber: input binary value.
    :return: output decimal value.
    :precondition: only accepts integer value.
    """
    myList = list(str(binaryNumber))
    myList = myList[::-1]
    length = len(myList)
    for i in range(length):
        myList[i] = int(myList[i])
    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (2**index)
    return decimalNumber

def hexadecimalToDecimal(hexadecimal):
    """
    This function converts hexadecimal to decimal.
    :param hexadecimal: input hexadecimal value.
    :return: output decimal value.
    :precondition: only accepts strings.
    """
    myList = list(hexadecimal)
    myList = myList[::-1]
    length = len(myList)
    for i in range(length):  # A10 B11 C12 D13 E14 F15
        if myList[i] == "A":
            myList[i] = 10
        elif myList[i] == "B":
            myList[i] = 11
        elif myList[i] == "C":
            myList[i] = 12
        elif myList[i] == "D":
            myList[i] = 13
        elif myList[i] == "E":
            myList[i] = 14
        elif myList[i] == "F":
            myList[i] = 15
    for j in range(length):
        myList[j] = int(myList[j])
    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (16 ** index)
    return decimalNumber

def hexadecimalToBinary(hexadecimal):
    """
    This function converts hexadecimal to binary.
    :param hexadecimal: input hexadecimal value.
    :return: output binary value.
    :precondition: only accepts strings.
    """
    myList = list(hexadecimal)
    length = len(myList)
    for i in range(length):
        if myList[i] == "A":
            myList[i] = 10
        elif myList[i] == "B":
            myList[i] = 11
        elif myList[i] == "C":
            myList[i] = 12
        elif myList[i] == "D":
            myList[i] = 13
        elif myList[i] == "E":
            myList[i] = 14
        elif myList[i] == "F":
            myList[i] = 15
    for j in range(length):
        myList[j] = int(myList[j])
    binaryNumber = ""
    for k in range(length):
        if myList[k] == 0:
            binaryNumber += "0000"
        elif myList[k] == 1:
            binaryNumber += "0001"
        elif myList[k] == 2:
            binaryNumber += "0010"
        elif myList[k] == 3:
            binaryNumber += "0011"
        elif myList[k] == 4:
            binaryNumber += "0100"
        elif myList[k] == 5:
            binaryNumber += "0101"
        elif myList[k] == 6:
            binaryNumber += "0110"
        elif myList[k] == 7:
            binaryNumber += "0111"
        elif myList[k] == 8:
            binaryNumber += "1000"
        elif myList[k] == 9:
            binaryNumber += "1001"
        elif myList[k] == 10:
            binaryNumber += "1010"
        elif myList[k] == 11:
            binaryNumber += "1011"
        elif myList[k] == 12:
            binaryNumber += "1100"
        elif myList[k] == 12:
            binaryNumber += "1100"
        elif myList[k] == 13:
            binaryNumber += "1101"
        elif myList[k] == 14:
            binaryNumber += "1110"
        elif myList[k] == 15:
            binaryNumber += "1111"
    return "0b" + str(binaryNumber)

def binaryToHexaDecimal(binaryNumber):
    """
    This function convert binary to hexadecimal.
    :param binaryNumber: input hexadecimal value
    :return: output decimal value
    :precondition: only accepts integer.
    """
    myList = list(str(binaryNumber))
    myList = myList[::-1]
    length = len(myList)
    for i in range(length):
        myList[i] = int(myList[i])
    decimalNumber = 0
    for index in range(length):
        decimalNumber += (myList[index]) * (2 ** index)
    return decimalToHex(decimalNumber)


def printMenu():
    """
    This function just prints the menu whenever called.
    """
    print("\nMenu: ")
    print("1. Decimal To Hex ")
    print("2. Hex To Decimal ")
    print("3. Decimal To Binary ")
    print("4. Binary To Decimal ")
    print("5. Binary To Hex ")
    print("6. Hex To Binary ")
    print("7. Quit ")

quit = False

while not quit:
    printMenu()
    command = int(input("\nWhich conversion would you like to perform?: "))
    print("Just straight away type your value, ex: 1101")
    if command == 1:
        decimalNumber = int(input("Please enter your decimal value: "))
        print(decimalToHex(decimalNumber))
    elif command == 2:
        hexadecimal = str(input("Please enter your hexadecimal value: "))
        print(hexadecimalToDecimal(hexadecimal))
    elif command == 3:
        decimalNumber = int(input("Please enter your decimal value: "))
        print(decimalToBinary(decimalNumber))
    elif command == 4:
        binaryNumber = int(input("Please enter your binary value: "))
        print(binaryToDecimal(binaryNumber))
    elif command == 5:
        binaryNumber = int(input("Please enter your binary value: "))
        print(binaryToHexaDecimal(binaryNumber))
    elif command == 6:
        hexadecimalNumber = str(input("Please enter your hexadecimal value: "))
        print(hexadecimalToBinary(hexadecimalNumber))
    elif command == 7:
        quit = True

