# This code request user's int a, b then checks whether the remainder is 0
a = int(input("What is integer a? "))
b = int(input("What is integer b? "))

if a % b == 0:
    print("True")   # Print true if the remainder is 0
else:
    print("False")  # Print false if the remainder is not 0