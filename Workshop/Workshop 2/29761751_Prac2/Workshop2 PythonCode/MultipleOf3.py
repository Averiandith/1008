# This code request user's integer than checks whether it is a multiple of 3
a = int(input("What is your integer? "))

if a % 3 == 0:
    print("True")   # Print True if it is a multiple of 3
else:
    print("False")  # Print False if it is not a multiple of 3


