# Ask for user's input a,b and seek the quotient and remainder
a = int(input("What's integer a: "))
b = int(input("What's integer b: "))

quotient = a // b
remainder = a % b

print("The quotient is: " + str(quotient))
print("The remainder is: " + str(remainder))