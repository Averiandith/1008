	.data
str1:	.asciiz	"What is your integer? "
str2:	.asciiz	"\nIt is a multiple of 3"
str3:	.asciiz	"\nIt is not a multiple of 3"
a:	.word	0
three:	.word	3
	.text
	
	addi	$v0, $zero, 4	# Print string "What is your integer?"
	la	$a0, str1
	syscall
		
	addi	$v0, $zero, 5	# Request for user's input
	syscall
	sw	$v0, a
	
	lw	$t0, a		# Load a, three to registers $t0, $t1 respectively
	lw	$t1, three
	div	$t0, $t1	# Divide $t0, $t1
	mfhi 	$t0		# Move value in HI (remainder) to $t0
	beq	$t0, $zero, prtT	# if $t0 == $0,jump to prtT

prtN:	addi	$v0, $zero, 4	# Print string "It is a multiple of 3"
	la	$a0, str3
	syscall
	j	end
		
prtT:	addi	$v0, $zero, 4	# Print string "It is not a multiple of 3"
	la	$a0, str2
	syscall
	
end:	addi	$v0, $zero, 10	# Terminate program
	syscall
	
