	.data
str1:	.asciiz	"Please enter your number: "
prtQ:	.asciiz	"Your Quotient is: "
prtR:	.asciiz	"\nYour Remainder is: "
a:	.word	0
b:	.word	0
retQ:	.word	0
retR:	.word	0
	.text
	
	addi	$v0, $zero, 4	# Prints string "Please enter your number: "
	la	$a0, str1
	syscall
	
	addi	$v0, $zero, 5	# Request user's input
	syscall
	sw	$v0, a

	addi	$v0, $zero, 4	# Prints string "Please enter your number: "
	la	$a0, str1
	syscall
				
	addi	$v0, $zero, 5	# Request user's input
	syscall
	sw	$v0, b
	
	lw	$t0, a		# load a,b into $t0, $t1 respectively
	lw	$t1, b
	div	$t0, $t1
	mflo	$t0		# Store value from HI to $t0
	mfhi	$t1		# Store value from LO to $t1
		
	sw	$t0, retQ	# Store word from $t0 to retQ
	sw	$t1, retR	# Store word from $t1 to retR
	
	addi	$v0, $zero, 4	# Print Quotient string
	la	$a0, prtQ
	syscall
	
	addi	$v0, $zero, 1	# Print Quotient value
	lw	$a0, retQ
	syscall
	
	addi	$v0, $zero, 4	# Print Remainder string
	la	$a0, prtR
	syscall
	
	addi	$v0, $zero, 1	# Print Remainder value
	lw	$a0, retR
	syscall
	
	addi	$v0, $zero, 10	# Terminate the program
	syscall
	
	
