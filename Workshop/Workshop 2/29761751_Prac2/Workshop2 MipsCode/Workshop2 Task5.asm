	.data
str1:	.asciiz	"Please enter integer a: "	
str2:	.asciiz	"\nPlease enter integer b: "
str3: 	.asciiz	"\nInteger a is a multiple of b "
str4:	.asciiz	"\nInteger a is not a multiple of b "
a:	.word	0
b:	.word	0
	.text
	
	addi	$v0, $zero, 4	# Print string "Please enter integer a: "
	la	$a0, str1
	syscall
	
	addi	$v0, $zero, 5	# Request user's input
	syscall
	sw	$v0, a	
	
	addi	$v0, $zero, 4	# Print string " Please enter integer b: "
	la	$a0, str2
	syscall
	
	addi	$v0, $zero, 5	# Request user's input
	syscall
	sw	$v0, b
	
	lw	$t0, a		# Load a,b into registers $t0, $t1 respectively
	lw	$t1, b
	div	$t0, $t1	# Divide $t0, $t1
	mfhi	$t0		# Move value in HI into $t0
	beq	$t0, $0, prtT	# If value of remainder == 0, jump to prtT
		
prtF:	addi	$v0, $zero, 4 	# Print string "Integer a is not a multiple of b"
	la	$a0, str4
	syscall
	j	end

prtT:	addi	$v0, $zero, 4	# Print string "Integer a is a multiple of b"
	la	$a0, str3
	syscall
	
end:	addi	$v0, $zero, 10	# Terminate the program
	syscall
