from Task_2 import ListArray
from Task_3 import ListArrayNode


def read_file(file_name):
	"""
	A function to read a file and append each line from file into an array
	:param file_name:
	:return array:
	"""
	array = ListArray(20)
	file = open(file_name)
	for line in file:
		array.append(line)
	return array


def read_file2(file_name):
	"""
	A function that reads a file and append each line from file into an array
	:param file_name:
	:return array:
	"""
	array = ListArrayNode(20)
	file = open(file_name)
	for line in file:
		array.append(line)
	return array

