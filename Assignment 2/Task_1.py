import referential_array


class MyListIterator:

    def __init__(self, my_list):
        """
        To initialize and set current to head
        :param my_list:
        """
        self.current = my_list.head

    def __iter__(self):
        """
        An iterator to return self
        :return self:
        """
        return self

    def __next__(self):
        """
        A next function to return current's item and set current to next
        :return item:
        """
        if self.current is None:
            raise StopIteration

        item = self.current.item
        self.current = self.current.next
        return item


class ListArray:
    def __init__(self, size):
        """
        Initializer for ListArray to store count, size and to build array
        :param size:
        """
        assert size > 0, "size must be > 0"
        self.count = 0
        self.size = size
        self.array = referential_array.build_array(size)

    def __str__(self):
        """
        Method to print result
        :return result: concatenation of items in array
        """
        result = ""
        for i in range(self.count):
            result = result + str(self.array[i]) + ","
        return result

    def __len__(self):
        """
        Method to produce length of array
        :return count:
        """
        return self.count

    def is_full(self):
        """
        Method to check if the array is full
        :return boolean whether it's True or False:
        """
        return self.count >= len(self.array)

    def __contains__(self, item):
        """
        If item is in array, return True, else return False
        :param item:
        :return True or False:
        """
        for i in range(self.count):
            if self.array[i] == item:
                return True
        return False

    def __getitem__(self, index):
        """
        Method to get item in the array by index
        :param index:
        :return item of the index in array:
        """
        if index >= self.count or index < -self.count:
            raise IndexError("Invalid index")

        if index >= 0:
            return self.array[index]

        elif index < 0:
            return self.array[self.count + index]

    def __setitem__(self, index, item):
        """
        Method to set given item into the array by given index
        :param index:
        :param item:
        """
        if index >= self.count or index < -self.count:
            raise IndexError("Invalid index")

        if index >= 0:
            self.array[index] = item

        if index < 0:
            self.array[self.count + index] = item

    def __eq__(self, other):
        """
        Method equal, if the whole array is the same as the other array return True, else return False
        :param other:
        :return True or False:
        """
        if self.array == other:
            return True
        return False

    def append(self, item):
        """
        Method to append item to the end of the array
        :param item:
        """
        if self.array == self.is_full():
            raise Exception("List is full")

        self.array[self.count] = item
        self.count += 1

    def insert(self, index, item):
        """
        Method insert to insert item at given index in array
        :param index:
        :param item:
        """
        if index >= self.count or index < -self.count:
            raise Exception("Invalid index")

        if index < 0:
            index = self.count + index
        for i in range(self.count, index, -1):
            self.array[i] = self.array[i - 1]
        self.array[index] = item
        self.count += 1

    def remove(self, item):
        """
        Method remove to remove target item in the array
        :param item:
        """
        if item not in self.array:
            raise Exception("Item not in")

        for i in range(self.count):
            if item == self.array[i]:
                for j in range(i, self.count - 1):
                    self.array[i] = self.array[i + 1]
                break
        self.count -= 1

    def delete(self, index):
        """
        Method to delete item at the given index of array
        :param index:
        """
        if abs(index) >= self.count:
            raise Exception("Invalid index")

        for i in range(index, self.count - 1):
            self.array[i] = self.array[i+1]
        self.count -= 1

    def sort(self, reverse):
        """
        Method to sort the array
        :param reverse: If reverse is True, sort the list in reverse
        """
        n = self.count
        if not reverse:
            for k in range(1, n):
                temp = self.array[k]
                i = k - 1
                while i >= 0 and self.array[i] > temp:
                    self.array[i + 1] = self.array[i]
                    i -= 1
                    self.array[i + 1] = temp

        else:
            for k in range(1, n):
                temp = self.array[k]
                i = k - 1
                while i >= 0 and self.array[i] < temp:
                    self.array[i + 1] = self.array[i]
                    i -= 1
                    self.array[i + 1] = temp

    def __iter__(self):
        """
        An iterator function
        """
        return MyListIterator(self)


if __name__ == "__main__":
    my_list = ListArray(5)
    my_list.append(5)
    my_list.append(6)
    my_list.append(7)
    my_list.append(8)
    print(my_list)
    print(len(my_list))
    for i in my_list:
        print(i)











