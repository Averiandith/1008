from Task_2 import ListArray


class Editor:

    def __init__(self):
        """
        Initialize the Editor class
        """
        self.list = ListArray()
        self.is_quit = False

        while not self.is_quit:
            self.var = input("FIT1008_text_editor (for HELP, type \"help\")>> ")
            self.var = self.var.split(" ", 1)
            if self.var[0] == "help":
                print(80*"=")
                print("\"insert num\" -- Insert a number at line \"num\" ")
                print(80 * "-")
                print("\"read filename\" -- Read the content of a file named \"filename\" ")
                print(80 * "-")
                print("\"write filename\" -- Save the file into \"filename\" file. ")
                print(80 * "-")
                print("\"print num1, num2\" -- Print the content of files in between lines \"num1\" and \"num2\". If no line numbers are given, the command will print the whole content. ")
                print(80 * "-")
                print("\"delete num\" -- Delete the line \"num\" from the file. If line \"num\" is not given, whole content would be deleted. ")
                print(80 * "-")
                print("\"search word\" -- It search \"word\" into the file. If found, it prints the line numbers, otherwise prints \"Not Found\". ")
                print(80 * "-")
                print("\"quit\" -- Quit from the program. ")
                print(80 * "=")

            elif self.var[0] == "insert":
                try:
                    var_num = int(self.var[1]) - 1
                except ValueError:
                    print("? (Error: Unknown command)")
                    continue
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue
                user_text = input("What is your desired text? ")
                try:
                    self.insert_num(var_num, user_text)
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue

            elif self.var[0] == "read":
                try:
                    file_name = self.var[1]
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue
                try:
                    self.read_filename(file_name)
                except FileNotFoundError:
                    print("?(Error: Unknown command)")
                    continue

            elif self.var[0] == "write":
                try:
                    file_name = self.var[1]
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue
                self.write_filename(file_name)

            elif self.var[0] == "print":
                try:
                    a_list = self.var[1].split(",")
                    num_1 = int(a_list[0])
                    num_2 = int(a_list[1])
                except ValueError:
                    print("? (Error: Unknown command)")
                    continue
                except IndexError:
                    for i in range(len(self.list)):
                        line_num = str(i + 1)
                        print(line_num + ">> " + str(self.list[i]))
                    continue
                if num_1 < len(self.list) and num_2 < len(self.list):
                    self.print_num1_num2(num_1, num_2)
                else:
                    print("? (Error: Unknown command)")
                    continue

            elif self.var[0] == "delete":
                try:
                    var_num = int(self.var[1])
                except ValueError:
                    print("? (Error: Unknown command)")
                    continue
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue
                try:
                    self.delete_num(var_num)
                except AttributeError:
                    print("? (Error: Unknown command)")
                    continue


            elif self.var[0] == "search":
                try:
                    var_num = self.var[1]
                except IndexError:
                    print("? (Error: Unknown command)")
                    continue
                self.search_word(var_num)

            elif self.var[0] == "quit":
                self.is_quit = True

    def insert_num(self, num, text):
        """
        To insert a line of user given text in the list before position num
        :param num:
        :param text:
        :return:
        """
        self.list.insert(num, text)

    def read_filename(self, file_name):
        """
        Read from a file and append each line of text into a list
        :param file_name:
        :return:
        """
        file = open(file_name, "r")
        for line in file:
            self.list.append(line.strip("\n"))
        file.close()

    def write_filename(self, file_name):
        """
        Creates a file and writes every item in the list into the file
        :param file_name:
        :return:
        """
        file = open(file_name, "w")
        for line in self.list:
            file.write(line+"\n")
        file.close()

    def print_num1_num2(self, num_1, num_2):
        """
        Prints the lines between positions num_1 and num_2
        :param num_1:
        :param num_2:
        :return:
        """
        num_1 = num_1 - 1
        num_2 = num_2 - 1
        if num_1 < num_2:
            for i in range(num_1, num_2):
                line_num = str(i + 1)
                print(line_num + ">> " + self.list[i])

    def delete_num(self, num):
        """
        Deletes the line of text in the list at position num
        :param num:
        :return:
        """
        self.list.delete(num - 1)

    def search_word(self, word):
        """
        Takes user's input word and prints the line numbers which the target word appears
        :param word:
        :return:
        """
        a_list = []
        for i in range(len(self.list)):
            array_word = self.list[i].lower().split(" ")
            word = word.lower()
            for k in range(len(array_word)):
                if array_word[k] == word:
                    a_list.append(i+1)
        for j in a_list:
            print(j)


editor = Editor()
