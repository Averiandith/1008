from unittest import TestCase
from Task_2 import ListArray

# Note To Tutor: Since Task 1 & 2 are similar except that Task 2 has a resizing array function,
# I chose to do testing for Task 2 only.


class TestListArray(TestCase):
    # test for boundary cases and normal cases

    def setUp(self):
        self.full_list = ListArray(20)
        for item in [23, 12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2]:
            self.full_list.append(item)

        self.normal_list = ListArray(20)
        for item in [234, 2, 5, 1]:
            self.normal_list.append(item)

    def test_is_full(self):
        is_full = self.full_list.is_full()  # Calling
        self.assertEqual(is_full, True)     # Checking

        normal_list = self.normal_list.is_full()
        self.assertEqual(normal_list, False)

    def test_append(self):
        self.normal_list.append(4)
        self.assertEqual(self.normal_list, [234, 2, 5, 1, 4])

        self.full_list.append(5)
        self.assertEqual(self.full_list, [23, 12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2, 5])
        self.assertEqual(len(self.full_list.array), 40)

    def test_insert(self):
        self.normal_list.insert(2, 69)
        self.assertEqual(self.normal_list, [234, 2, 69, 5, 1])

        self.full_list.insert(2, 69)
        self.assertEqual(self.full_list, [23, 12, 69, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_remove(self):
        self.normal_list.remove(5)
        self.assertEqual(self.normal_list, [234, 2, 1])

        self.full_list.remove(12)
        self.assertEqual(self.full_list, [23, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_delete(self):
        self.normal_list.delete(2)
        self.assertEqual(self.normal_list, [234, 2, 1])

        self.full_list.delete(0)
        self.assertEqual(self.full_list, [12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_sort(self):
        self.normal_list.sort(False)
        self.assertEqual(self.normal_list, [1, 2, 5, 234])

        self.full_list.sort(True)
        self.assertEqual(self.full_list, [886, 564, 543, 357, 346, 326, 64, 53, 23, 23, 23, 13, 12, 12, 12, 12, 12, 8, 3, 2])

    def test_update_new_array(self):
        self.normal_list.append(45)
        self.assertEqual(len(self.normal_list.array), 20)

        self.normal_list.delete(2)
        self.assertEqual(len(self.normal_list.array), 20)

        self.full_list.append(69)
        self.assertEqual(len(self.full_list.array), 40)

        self.full_list.delete(2)
        self.assertEqual(len(self.full_list.array), 40)


