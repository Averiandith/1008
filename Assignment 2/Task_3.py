from node import Node


class MyListIterator:

    def __init__(self, my_list):
        """
        To initialize and set current to head
        :param my_list:
        """
        self.current = my_list.head

    def __iter__(self):
        """
        An iterator to return self
        :return self:
        """
        return self

    def __next__(self):
        """
        A next function to return current's item and set current to next
        :return item:
        """
        if self.current is None:
            raise StopIteration

        item = self.current.item
        self.current = self.current.next
        return item


class ListArrayNode:
    def __init__(self, size=20):
        """
        Initializer for ListArray to store instance variables
        :param size:
        """
        assert size > 0, "size must be > 0"
        self.count = 0
        self.size = size
        self.head = None

    def is_empty(self):
        """
        A method that checks whether the array is empty
        :return boolean to return True or False:
        """
        return self.count == 0

    def is_full(self):
        """
        Method to check whether the array is full
        :return boolean True or False, but since array node implementation is never full, so it only returns False:
        """
        return False

    def __len__(self):
        """
        Method to return the length of the array
        :return count:
        """
        return self.count

    def get_node(self, index):
        """
        Method to get the node of the index
        :param index: index of the node
        :return node: the node of the index
        """
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")

        node = self.head
        counter = 0
        while counter < index:
            node = node.next
            counter += 1
        return node

    def __str__(self):
        """
        Method to print result
        :return result: concatenation of items in array
        """
        result = ""
        current = self.head
        while current is not None:
            result = result + str(current.item) + ","
            current = current.next
        return result

    def __contains__(self, item):
        """
        If item is in array, return True else return False
        :param item:
        :return True or False:
        """
        current_node = self.head
        for i in range(self.count):
            if current_node.item == item:
                return True
            current_node = current_node.next
        return False

    def __getitem__(self, index):
        """
        Method to get item in the array by index
        :param index:
        :return: item of the index in array
        """
        if index >= self.count or index < -self.count:
            raise IndexError("Invalid index")
        if index >= 0:
            node = self.get_node(index)
        elif index < 0:
            node = self.get_node(self.count + index)
        return node.item

    def __setitem__(self, index, item):
        """
        Method to set given item into the array by given index
        :param index:
        :param item:
        """
        if index >= self.count or index < -self.count:
            raise IndexError("Invalid index")
        if index >= 0:
            self.get_node(index).item = item
        if index < 0:
            self.get_node(self.count + index).item = item

    def __eq__(self, other):
        """
        Method equal, if the whole array is the same as the other array return True, else return False
        :param other:
        :return True or False:
        """
        if self.count != len(other):
            return False
        else:
            current = self.head
            for i in range(self.count):
                if other[i] != current.item:
                    return False
                current = current.next
            return True

    def append(self, item):
        """
        Method to append item to the end of the array
        :param item:
        """
        if self.is_empty():
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(len(self) - 1)
            cur_node.next = Node(item, cur_node.next)
        self.count += 1

    def insert(self, index, item):
        """
        Method insert, to insert item at given index in array
        :param index:
        :param item:
        """
        if index < 0:
            index = self.count + index
        if index == 0:
            self.head = Node(item, self.head)
        else:
            node = self.get_node(index - 1)
            node.next = Node(item, node.next)
        self.count += 1

    def remove(self, item):
        """
        Method to remove target item in the array
        :param item:
        """
        if not self.__contains__(item):
            raise Exception("Item not in")

        current_node = self.head
        for i in range(self.count):
            if i == 0 and current_node.item == item:
                self.head = current_node.next
                self.count -= 1
                break
            elif current_node.item == item:
                node = self.get_node(i - 1)
                node.next = current_node.next
                self.count -= 1
                break
            current_node = current_node.next

    def delete(self, index):
        """
        Method to delete item at the given index of array
        :param index:
        """
        if self.is_empty():
            raise IndexError("The list is empty")
        if index < 0:
            index = self.count + index
        if index == 0:
            self.head = self.head.next
        else:
            node = self.get_node(index - 1)
            node.next = node.next.next
        self.count -= 1

    def sort(self, reverse=False):
        """
        Method to sort the array
        :param reverse: If reverse is True, sort the list in reverse
        """
        n = self.count
        if not reverse:
                for k in range(1, n):
                    temp = self.get_node(k).item
                    i = k - 1
                    while i >= 0 and self.get_node(i).item > temp:
                        node_one = self.get_node(i)
                        self.delete(i)
                        self.insert(i + 1, node_one.item)
                        i -= 1

        else:
            for k in range(1, n):
                temp = self.get_node(k).item
                i = k - 1
                while i >= 0 and self.get_node(i).item < temp:
                    node_one = self.get_node(i)
                    self.delete(i)
                    self.insert(i + 1, node_one.item)
                    i -= 1

    def ___iter__(self):
        """
        A function that iterates the array
        """
        return MyListIterator(self)


if __name__ == "__main__":
    a_list = [23, 12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2]
    my_list = ListArrayNode(20)
    for i in range(len(a_list)):
        my_list.append(a_list[i])
    print(my_list)
    my_list.sort()
    print(my_list)
    my_list.sort(True)
    print(my_list)








