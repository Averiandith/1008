from unittest import TestCase
from Task_4 import *


class TestReadFile(TestCase):

    def test_read_file(self):
        array = read_file("testing.txt")
        self.assertEqual(array, ["hello\n", "hello"])

        array = read_file("testing2.txt")
        self.assertEqual(array, ["hello\n", "how are you"])

    def test_read_file2(self):
        array = read_file2("testing.txt")
        self.assertEqual(array, ["hello\n", "hello"])

        array = read_file2("testing2.txt")
        self.assertEqual(array, ["hello\n", "how are you"])


