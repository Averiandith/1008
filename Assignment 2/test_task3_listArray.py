from unittest import TestCase
from Task_3 import ListArrayNode



class TestListArrayNode(TestCase):
    def setUp(self):
        self.full_list = ListArrayNode(20)
        for item in [23, 12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2]:
            self.full_list.append(item)

        self.normal_list = ListArrayNode(20)
        for item in [234, 2, 5, 1]:
            self.normal_list.append(item)

    def test_is_empty(self):
        is_full = self.full_list.is_empty()
        self.assertEqual(is_full, False)

        normal_list = self.normal_list.is_empty()
        self.assertEqual(normal_list, False)

    def test_is_full(self):
        is_full = self.full_list.is_full()
        self.assertEqual(is_full, False)

        normal_list = self.normal_list.is_full()
        self.assertEqual(normal_list, False)

    def test_get_node(self):
        node = self.normal_list.get_node(0)
        self.assertEqual(node.item, 234)

        node = self.full_list.get_node(19)
        self.assertEqual(node.item, 2)

    def test_append(self):
        self.normal_list.append(5)
        self.assertEqual(self.normal_list, [234, 2, 5, 1, 5])

        self.full_list.append(5)
        self.assertEqual(self.full_list, [23, 12, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2, 5])

    def test_insert(self):
        self.normal_list.insert(1, 69)
        self.assertEqual(self.normal_list, [234, 69, 2, 5, 1])

        self.full_list.insert(2, 69)
        self.assertEqual(self.full_list, [23, 12, 69, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_remove(self):
        self.normal_list.remove(234)
        self.assertEqual(self.normal_list, [2, 5, 1])

        self.full_list.remove(326)
        self.assertEqual(self.full_list, [23, 12, 346, 13, 357, 12, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_delete(self):
        self.normal_list.delete(0)
        self.assertEqual(self.normal_list, [2, 5, 1])

        self.full_list.delete(1)
        self.assertEqual(self.full_list, [23, 346, 13, 357, 12, 326, 12, 564, 12, 543, 12, 64, 23, 886, 23, 53, 3, 8, 2])

    def test_sort(self):
        self.normal_list.sort()
        self.assertEqual(self.normal_list, [1, 2, 5, 234])

        self.full_list.sort(True)
        self.assertEqual(self.full_list, [886, 564, 543, 357, 346, 326, 64, 53, 23, 23, 23, 13, 12, 12, 12, 12, 12, 8, 3, 2])




