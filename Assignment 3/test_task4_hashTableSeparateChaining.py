from unittest import TestCase
from Task_4 import HashTableSeparateChaining


class TestHashTableSeparateChaining(TestCase):
    def setUp(self):
        self.ht = HashTableSeparateChaining(1)
        self.ht["Sunny"] = "Rainy"
        self.ht["True"] = False
        self.ht["Number"] = 111
        self.ht.utility()

    def test__getitem__(self):
        self.assertEqual(self.ht["Sunny"], "Rainy")
        self.assertEqual(self.ht["True"], False)
        self.assertEqual(self.ht["Number"], 111)

    def test__setitem__(self):
        self.ht["Sunny"] = "Thunder"
        self.assertEqual(self.ht["Sunny"], "Thunder")

        self.ht["True"] = "Truer than you"
        self.assertEqual(self.ht["True"], "Truer than you")

        self.ht["Number"] = 456
        self.assertEqual(self.ht["Number"], 456)

    def test__contains__(self):
        x = "Sunny" in self.ht
        self.assertEqual(x, True)

        y = "True" in self.ht
        self.assertEqual(y, True)

        z = "Number" in self.ht
        self.assertEqual(z, True)

    def test_utility(self):
        x = self.ht.collision
        self.assertEqual(x, 2)


