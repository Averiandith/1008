/import referential_array
import timeit
from node import Node


class HashTableSeparateChaining:
    def __init__(self, size):
        """
        Initializer for HashTable
        :complexity: O(N), where N is the complexity for build array item
        :param size: Size of the list
        :raises: Nothing
        """
        self.array = referential_array.build_array(size)
        self.count = 0
        self.table_size = size
        self.collision = 0
        self.avg_probe_length = 0
        self.load = 0

    def hash(self, key):
        """
        A function that receives a key and convert it to a position (position depends on a & table_size)
        :complexity: 0(N) best & worst case, where N is the number of character in a key
        :param key: A key
        :raises: Nothing
        :return h: Return a hash key (position)
        """
        h = 0
        a = 11
        for c in key:
            h = (h * a + ord(c)) % self.table_size
        return h

    def __len__(self):
        """
        Dunder method to return length of HashTable
        :complexity: O(1) due to return
        :raises: Nothing
        :return: Length of the HashTable
        """
        return self.count

    def __str__(self):
        """
        Dunder method to print HashTable
        :complexity: O(N), where N is the size of the table
        :return res: Result(s) in the HashTable
        """
        res = ""
        for i in range(self.table_size):
            if self.array[i] is not None:

                current = self.array[i]
                while current is not None:
                    res = res + "(" + str(current.item[0]) + ", " + str(current.item[1]) + ")\n"
                    current = current.next
        return res

    def __setitem__(self, key, data):
        """
        Dunder method to set value in HashTable based on key value
        :complexity: O(1), When key location is empty, O(N) when table is full and key doesn't exist
        :param key: Key to insert in HashTable
        :param value: Value to store in the key position
        :raises Exception: After looping table and finds that table is full, unable to insert key
        :return none:
        """
        hash_value = self.hash(key)

        if self.array[hash_value] is None:
            head = Node((key, data), None)
            self.array[hash_value] = head
            self.count += 1
            return
        else:
            current = self.array[hash_value]
            while current is not None:
                if current.item[0] == key:
                    current.item = (key, data)
                    return
                current = current.next
            self.array[hash_value] = Node((key, data), self.array[hash_value])  # Node((key, data), point to old node)
            self.collision += 1
            self.count += 1
            return

    def __getitem__(self, key):
        """
         Dunder method to get item in HashTable
         :complexity: O(1), When the item is already at hash position, O(N) when hash table is full and key doesn't not exist.
        :param key:
        :raises Exception: When key is not found in table
        :return value: If value is present, return value based on key
        """
        hash_value = self.hash(key)
        if self.array[hash_value] is None:
            raise Exception("key not found")
        else:
            current = self.array[hash_value]
            while current is not None:
                if current.item[0] == key:
                    return current.item[1]
                current = current.next
        raise Exception("Key not found")

    def __contains__(self, key):
        """
        Dunder methods that return true if key exists in HashTable and vice versa
        :complexity: O(1), when item is at key position, O(N) when table is full and key is not in
        :param key: User's input key
        :raises: Nothing
        :return: Either True or False
        """
        hash_value = self.hash(key)

        if self.array[hash_value] is None:
            return False
        else:
            current = self.array[hash_value]
            while current is not None:
                if current.item[0] == key:
                    return True
                current = current.next
        return False

    def utility(self):
        """
         An utility function that prints total collision, average probe length and load for hashTable whenever called
         :complexity: O(1)
         :param: None
         :raises: None
         :return: None
         """
        self.avg_probe_length = self.collision / self.count
        self.load = self.count / self.table_size
        print("The total collision is: " + str(self.collision))
        print("The average probe length will be: " + str(self.avg_probe_length))
        print("The load is: " + str(self.load))


"""
Briefing:
I used "english_small.txt" to load into both of my SeparateChaining and LinearProbing functions 
to make a comparision. Both a values will be 11 and table_size will be 399989.

Findings:
Collisions: 8685 (Separate) vs 11773 (Linear)
Average Probe Length: 0.1032 (Separate) vs 0.1399 (Linear)
Time: 0.6534 (Separate) vs 0.65 (Linear)

Analysis:
Due to the setitem difference in separate chaining, (when collision were to occur, chain item in hash table)
there will be less collisions and average probe length in separate chaining comparing to linear probing.
However, the reason that there is a slight increase of time in chaining is due to the extra time needed to chain
the items. Furthermore, separate chaining avoid primary clustering and have less collisions because if there
is a collision, it will be assigned in a chain. 

"""

HT = HashTableSeparateChaining(399989)
file = open("english_small.txt")
start_time = timeit.default_timer()
for line in file:
    HT[line] = line
taken = timeit.default_timer() - start_time
print("File One (english_small.txt)")
print("Time taken: " + str(taken))
HT.utility()

"""
if __name__ == "__main__":
    HT = HashTableSeparateChaining(1)
    HT["Sunny"] = "Rainy"
    HT["True"] = False
    HT["Number"] = 111
    print(HT)
    print(len(HT))
    HT.utility()
"""