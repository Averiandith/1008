import referential_array
import timeit


class HashTableLinearProbingWithChains:
    def __init__(self, size):
        """
        Initializer for HashTable
        :complexity: 0(N), where N is the complexity for build array item
        :param size: Size of the list
        :raises: None
        """
        self.array = referential_array.build_array(size)
        self.count = 0
        self.table_size = size
        self.collision = 0
        self.avg_probe_length = 0
        self.load = 0

    def hash(self, key):
        """
        A function that receives a key and convert it to a position (position depends on a & table_size)
        :complexity: 0(N) best & worst case, where N is the number of character in a key
        :param key: A key
        :raises: Nothing
        :return h: Return a hash key (position)
        """
        h = 0
        a = 101
        for c in key:
            h = (h * a + ord(c)) % self.table_size
        return h

    def __len__(self):
        """
        Dunder method to return length of HashTable
        :complexity: O(1) due to return
        :raises: Nothing
        :return: Length of the HashTable
        """
        return self.count

    def __str__(self):
        """
        Dunder method to print HashTable
        :complexity: O(N), where N is the size of the table
        :return res: Result(s) in the HashTable
        """
        res = ""
        for i in range(self.table_size):
            if self.array[i] is not None:
                res = res + "(" + str(self.array[i][0]) + "," + str(self.array[i][1]) + ")\n"
        return res

    def __getitem__(self, key):
        """
         Dunder method to get item in HashTable
         :complexity: O(1), When the item is already at hash position, O(N) when hash table is full and key doesn not exist.
        :param key:
        :raises KeyError: When there is no value in the position in HashTable
        :return value: If value is present, return value based on key
        """
        hash_value = self.hash(key)

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                raise KeyError("Key doesn't exists")
            elif self.array[hash_value][0] == key:
                return self.array[hash_value][1]
            else:
                hash_value = (hash_value + 1) % self.table_size
        raise KeyError("Key doesn't exists")

    def __contains__(self, key):
        """
        Dunder methods that return true if key exists in HashTable and vice versa
        :complexity: O(1), when item is at key position, O(N) when table is full and key is not in
        :param key: User's input key
        :raises: Nothing
        :return: Either True or False
        """
        hash_value = self.hash(key)

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                return False
            elif self.array[hash_value][0] == key:
                return True
            else:
                hash_value = (hash_value + 1) % self.table_size
        return False  # After going through one loop, nothing then return False

    def __setitem__(self, key, value):
        """
        Dunder method to set value in HashTable based on key value
        :complexity: O(1), When key location is empty, O(N) when table is full and key doesn't exist
        :param key: Key to insert in HashTable
        :param value: Value to store in the key position
        :raises Exception: After looping table and finds that table is full, unable to insert key
        :return none:
        """
        hash_value = self.hash(key)
        collision = 0

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                self.array[hash_value] = (key, value)
                self.collision += collision
                self.count += 1
                return
            elif self.array[hash_value][0] == key:
                self.array[hash_value] = (key, value)
                return
            else:
                hash_value = (hash_value + 1) % self.table_size
                collision += 1
        raise Exception("Table is full! & key does not exist in table yet.")

    def utility(self):
        """
        An utility function that prints total collision, average probe length and load for hashTable whenever called
        :complexity: O(1)
        :param: None
        :raises: None
        :return: None
        """
        self.avg_probe_length = self.collision / self.count
        self.load = self.count / self.table_size
        print("The total collision is: " + str(self.collision))
        print("The average probe length will be: " + str(self.avg_probe_length))
        print("The load is: " + str(self.load))


if __name__ == "__main__":
    HT = HashTableLinearProbingWithChains(3)
    HT["Sunny"] = "Rainy"
    HT["True"] = False
    HT["Number"] = 111
    HT.utility()

    # Below is how I test my 3 files for analysis table in Excel

    HT = HashTableLinearProbingWithChains(399989)
    file = open("english_small.txt")                            # File 1
    start_time = timeit.default_timer()
    for line in file:
        HT[line] = line
    taken = timeit.default_timer() - start_time
    print("File One (english_small.txt)")
    print("Time taken: " + str(taken))
    HT.utility()

    HT_two = HashTableLinearProbingWithChains(347989)
    file = open("english_large.txt")                            # File 2
    start_time = timeit.default_timer()
    for line in file:
        HT_two[line] = line
    taken = timeit.default_timer() - start_time
    print("\n")
    print("File Two (english_large.txt)")
    print("Time taken: " + str(taken))
    HT_two.utility()

    HT_three = HashTableLinearProbingWithChains(309293)
    file = open("french.txt")                                   # File 3
    start_time = timeit.default_timer()
    for line in file:
        HT_three[line] = line
    taken = timeit.default_timer() - start_time
    print("\n")
    print("File Three (french.txt)")
    print("Time taken: " + str(taken))
    HT_three.utility()


