from unittest import TestCase
from Task_1 import HashTableLinearProbing


class TestHashTableLinearProbing(TestCase):
    def setUp(self):
        self.ht = HashTableLinearProbing(size=3)
        self.ht["Sunny"] = "Rainy"
        self.ht["True"] = False
        self.ht["Number"] = 111

    def test__getitem__(self):
        self.assertEqual(self.ht["Sunny"], "Rainy")
        self.assertEqual(self.ht["True"], False)
        self.assertEqual(self.ht["Number"], 111)
        self.assertRaises(KeyError, lambda: self.ht["Why"])

    def test__setitem__(self):
        self.ht["Sunny"] = "Cloudy"
        self.assertEqual(self.ht["Sunny"], "Cloudy")

        self.ht["True"] = True
        self.assertEqual(self.ht["True"], True)

        self.ht["Number"] = 123
        self.assertEqual(self.ht["Number"], 123)

        with self.assertRaises(Exception):
            self.ht["Why"] = 456

    def test__contains__(self):
        x = "Sunny" in self.ht
        self.assertEqual(x, True)

        y = "True" in self.ht
        self.assertEqual(y, True)

        z = "Number" in self.ht
        self.assertEqual(z, True)

