from unittest import TestCase
from Task_3 import HashTableQuadraticProbingWithChains


class TestHashTableQuadraticProbingWithChains(TestCase):
    def setUp(self):
        self.ht = HashTableQuadraticProbingWithChains(3)
        self.ht["Sunny"] = "Rainy"
        self.ht["True"] = False
        self.ht["Number"] = 111
        self.ht.utility()

    def test__getitem__(self):
        self.assertEqual(self.ht["Sunny"], "Rainy")
        self.assertEqual(self.ht["True"], False)
        self.assertEqual(self.ht["Number"], 111)
        self.assertRaises(KeyError, lambda: self.ht["Why"])

    def test__setitem__(self):
        self.ht["Sunny"] = "Cloudy"
        self.assertEqual(self.ht["Sunny"], "Cloudy")

        self.ht["True"] = True
        self.assertEqual(self.ht["True"], True)

        self.ht["Number"] = 123
        self.assertEqual(self.ht["Number"], 123)

    def test__contains__(self):
        x = "Sunny" in self.ht
        self.assertEqual(x, True)

        y = "True" in self.ht
        self.assertEqual(y, True)

        z = "Number" in self.ht
        self.assertEqual(z, True)

    def test_utility(self):
        x = self.ht.collision
        self.assertEqual(x, 1)

    def test_rehash(self):
        ht2 = HashTableQuadraticProbingWithChains(3)

        ht2["Rainy"] = "Nothing"
        ht2["Why"] = "Maybe"
        ht2["Umbrella"] = "Really"

        self.assertEqual(ht2.table_size, 7)
        self.assertEqual(ht2["Rainy"], "Nothing")
        self.assertEqual(ht2["Why"], "Maybe")
        self.assertEqual(ht2["Umbrella"], "Really")
