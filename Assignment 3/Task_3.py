# I chose to use Task_2 to modify to Quadratic Probing, since Task_1 is the same
# Task 3's HashTable is also used for Task 5, which is why I have an iter class.
import referential_array
import timeit


class HashTableQuadraticProbingWithChains:
    def __init__(self, size):
        """+-
        Initializer for HashTable
        :complexity: 0(N), where N is the complexity for build array item
        :param size: Size of the list
        :raises: Nothing
        """
        self.array = referential_array.build_array(size)
        self.count = 0
        self.table_size = size
        self.collision = 0
        self.avg_probe_length = 0
        self.load = 0
        self.rehash_value = 0

    def hash(self, key):
        """
        A function that receives a key and convert it to a position (position depends on a & table_size)
        :complexity: 0(N) best & worst case, where N is the number of character in a key
        :param key: A key
        :raises: Nothing
        :return h: Return a hash key (position)
        """
        h = 0
        a = 101
        for c in key:
            h = (h * a + ord(c)) % self.table_size
        return h

    def __len__(self):
        """
        Dunder method to return length of HashTable
        :complexity: O(1) due to return
        :raises: Nothing
        :return: Length of the HashTable
        """
        return self.count

    def __str__(self):
        """
        Dunder method to print HashTable
        :complexity: O(N), where N is the size of the table
        :return res: Result(s) in the HashTable
        """
        res = ""
        for i in range(self.table_size):
            if self.array[i] is not None:
                res = res + "(" + str(self.array[i][0]) + "," + str(self.array[i][1]) + ")\n"
        return res

    def __getitem__(self, key):
        """
         Dunder method to get item in HashTable
         :complexity: O(1), When the item is already at hash position, O(N) when hash table is full and key doesn not exist.
        :param key:
        :raises KeyError: When there is no value in the position in HashTable
        :return value: If value is present, return value based on key
        """
        i = 0
        h = self.hash(key)
        hash_value = h + i ** 2

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                raise KeyError("Key doesn't exists")
            elif self.array[hash_value][0] == key:
                return self.array[hash_value][1]
            else:
                i += 1
                hash_value = (h + i ** 2) % self.table_size
        raise KeyError("Key doesn't exists")

    def __contains__(self, key):
        """
        Dunder methods that return true if key exists in HashTable and vice versa
        :complexity: O(1), when item is at key position, O(N) when table is full and key is not in
        :param key: User's input key
        :raises: Nothing
        :return: Either True or False
        """
        i = 0
        h = self.hash(key)
        hash_value = h + i ** 2

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                return False
            elif self.array[hash_value][0] == key:
                return True
            else:
                i += 1
                hash_value = (h + i ** 2) % self.table_size
        return False  # After going through one loop, nothing then return False

    def __setitem__(self, key, value):
        """
        Dunder method to set value in HashTable based on key value
        :complexity: O(1), When key location is empty, O(N) when table is full and key doesn't exist
        :param key: Key to insert in HashTable
        :param value: Value to store in the key position
        :raises Exception: After looping table and finds that table is full, unable to insert key
        :return none:
        """
        i = 0
        h = self.hash(key)
        hash_value = h + i ** 2
        collision = 0

        for _ in range(self.table_size):
            if self.array[hash_value] is None:
                self.array[hash_value] = (key, value)
                self.collision += collision
                self.count += 1
                if self.count >= 2*self.table_size/3:
                    print(self.table_size)
                    self.rehash()
                    print(self.table_size)
                return
            elif self.array[hash_value][0] == key:
                self.array[hash_value] = (key, value)       # I intentionally want to replace the item, so no collision
                return
            else:
                i += 1
                hash_value = (h + i**2) % self.table_size
                collision += 1
        raise Exception("Table is full! & key does not exist in table yet.")

    def __iter__(self):
        """
        Dunder method to iterate through array
        :return: An iterator for array
        """
        return Iterator(self.array)

    def utility(self):
        """
        An utility function that prints total collision, average probe length and load for hashTable whenever called
        :param: None
        :raises: None
        :return: None
        """
        self.avg_probe_length = self.collision / self.count
        self.load = self.count / self.table_size
        print("The total collision is: " + str(self.collision))
        print("The average probe length will be: " + str(self.avg_probe_length))
        print("The load is: " + str(self.load))

    def rehash(self):
        """
        A method to rehash my HashTable when my current size is larger or equal than 2*self.table_size/3
        :complexity: O(N) where N is the table size
        :raises: None
        :return: None
        """
        old_array = self.array
        self.rehash_value = self.table_size * 2 + 1
        self.__init__(self.rehash_value)                    # New array

        for item in old_array:
            if item is not None:
                key, value = item
                self[key] = value


class Iterator:
    def __init__(self, array):
        """
        Initializer for iterator class
        :param array:
        """
        self.array = array
        self.current_index = 0

    def __iter__(self):
        """
        Dunder method to return self
        :return: self
        """
        return self

    def __next__(self):
        """
        Dunder method to return element
        :complexity: O(1)
        :return: item
        """
        if self.current_index >= len(self.array):
            raise StopIteration
        else:
            item = self.array[self.current_index]
            self.current_index += 1
            return item

"""
Briefing:
I used "english_small.txt" to load into both of my QuadraticProbing and LinearProbing functions 
to make a comparision. Both a values will be 11 and table_size will be 399989.

Findings:
Collisions: 10011 (Quadratic) vs 11773 (Linear)
Average Probe Length: 0.1369 (Quadratic) vs 0.1399 (Linear)
Time: 0.75511 (Quadratic) vs 0.65 (Linear)

Analysis:
Due to the setitem difference in quadratic probing (erratic jumping behavior when setting item in hash table)
there will be less collisions and average probe length in quadratic probing comparing to linear probing.
However, the reason that there is a slight increase of time in quadratic is due to the extra time needed to compute
the quadratic steps. Furthermore, quadratic probing avoid primary clustering 

"""

if __name__ == "__main__":
    HT = HashTableQuadraticProbingWithChains(3)
    HT["Kruze"] = "bookname"
    HT["ada"] = "hello"
    HT["ada"] = "helo"
    HT["ada"] = "helloo"
    HT["adda"] = "hello"
    HT["addddda"] = "WAHTTTTTT"
    print(HT)
    print(HT["ada"])            # __getitem__
    HT.utility()                # utility
    print("addaa" in HT)        # __contains__
    print("Kruze" in HT)

    HT2 = HashTableQuadraticProbingWithChains(3)
    HT2["Sunny"] = "Rainy"
    HT2["True"] = False
    HT2["Number"] = 111
    HT2.utility()
    print(HT2)

    HT = HashTableQuadraticProbingWithChains(399989)
    file = open("english_small.txt")
    start_time = timeit.default_timer()
    for line in file:
        HT[line] = line
    taken = timeit.default_timer() - start_time
    print("File One (english_small.txt)")
    print("Time taken: " + str(taken))
    HT.utility()



