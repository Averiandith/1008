from unittest import TestCase
from Task_5 import *


class TestTable(TestCase):
    def setUp(self):
        self.ht1 = table("e_book_test1.txt")
        self.ht2 = table("e_book_test2.txt")
        self.ht3 = table("e_book_test3.txt")
        self.word1 = "none"
        self.word2 = "a"
        self.word3 = "fag"

    def test_WordChecker(self):
        x = word_checker(self.word1, self.ht1)
        self.assertEqual(x, "Word is uncommon.")

        y = word_checker(self.word2, self.ht2)
        self.assertEqual(y, "Word is common.")

        z = word_checker(self.word3, self.ht3)
        self.assertEqual(z, "Word is rare.")

