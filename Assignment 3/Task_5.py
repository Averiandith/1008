import string
from Task_3 import HashTableQuadraticProbingWithChains


def table(file_name):
    """
    A function to create my hashTable for e_book while using QuadraticProbing
    :complexity: O(N^2) worst case, where N is size of file, O(1) best case when file is empty
    :raises: None
    :param file_name: A filename of choice
    :return ht: hashTable
    """
    ht = HashTableQuadraticProbingWithChains(39998)
    file = open(file_name, "r")
    for line in file:
        for item in string.punctuation:
            line = line.replace(item, "")
        for word in line.strip("\n").split():
            word = word.lower()
            key = word
            if key in ht:
                ht[key] += 1
            else:
                ht.__setitem__(key, 1)
    return ht


def get_max_frequency(ht):
    """
    A function that get the maximum occurrence of a word in a file
    :complexity: O(N) worst case N is size of file, O(1) best case when file is empty
    :raises: None
    :param ht:
    :return max_frequency[1]: maximum frequency of word
    """
    max_frequency = [0, 0]
    for item in ht:
        if item is not None:
            if item[1] > max_frequency[1]:
                max_frequency[0] = item[0]
                max_frequency[1] = item[1]
    return max_frequency[1]


def word_checker(word, hash_table):
    """
    A function that return the result of word whether word is common/uncommon or rare
    :complexity: O(1)
    :param word: Word to check in hash_table
    :param hash_table:
    :return: return 3 result
    :raises KeyError: raises KeyError when word is not in table
    """
    max_frequency = get_max_frequency(hash_table)
    try:
        frequency = hash_table[word]
    except KeyError:
        print("Word is not inside table, frequency = 0")
        frequency = 0
    if frequency >= max_frequency/100:
        return "Word is common."

    elif frequency >= max_frequency/1000:
        return "Word is uncommon."

    else:
        return "Word is rare."


def initialize_file_1():
    ht1 = table("e_book_test1.txt")
    user_word = input("What is your desired word? ").lower()
    print(word_checker(user_word, ht1))
    print("\n")


def initialize_file_2():
    ht1 = table("e_book_test2.txt")
    user_word = input("What is your desired word? ").lower()
    print(word_checker(user_word, ht1))
    print("\n")


def initialize_file_3():
    ht1 = table("e_book_test3.txt")
    user_word = input("What is your desired word? ").lower()
    print(word_checker(user_word, ht1))
    print("\n")


if __name__ == "__main__":
    initialize_file_1()
    initialize_file_2()
    initialize_file_3()

