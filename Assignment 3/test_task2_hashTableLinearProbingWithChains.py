from unittest import TestCase
from Task_2 import HashTableLinearProbingWithChains


class TestHashTableLinearProbingWithChains(TestCase):
    def setUp(self):
        self.ht = HashTableLinearProbingWithChains(size=3)
        self.ht["Sunny"] = "Rainy"
        self.ht["True"] = False
        self.ht["Number"] = 111
        self.ht.utility()

    def test__getitem__(self):
        self.assertEqual(self.ht["Sunny"], "Rainy")
        self.assertEqual(self.ht["True"], False)
        self.assertEqual(self.ht["Number"], 111)
        self.assertRaises(KeyError, lambda: self.ht["Why"])

    def test__setitem__(self):
        self.ht["Sunny"] = "Cloudy"
        self.assertEqual(self.ht["Sunny"], "Cloudy")

        self.ht["True"] = True
        self.assertEqual(self.ht["True"], True)

        self.ht["Number"] = 123
        self.assertEqual(self.ht["Number"], 123)

        with self.assertRaises(Exception):
            self.ht["Why"] = 456

    def test__contains__(self):
        x = "Sunny" in self.ht
        self.assertEqual(x, True)

        y = "True" in self.ht
        self.assertEqual(y, True)

        z = "Number" in self.ht
        self.assertEqual(z, True)

    def test_utility(self):
        x = self.ht.collision
        self.assertEqual(x, 2)

        y = self.ht.load
        self.assertEqual(y, 1.0)




