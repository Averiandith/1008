import referential_array


class HashTableLinearProbing:
        def __init__(self, size):
            """
            Initializer for HashTable
            :complexity: 0(N), where N is the complexity for build array item
            :param size: Size of the list
            :raises: None
            """
            self.array = referential_array.build_array(size)
            self.count = 0
            self.table_size = size

        def hash(self, key):
            """
            A function that receives a key and convert it to a position (position depends on a & table_size)
            :complexity: 0(N) best & worst case, where N is the number of character in a key
            :param key: A key
            :raises: None
            :return h: Return a hash key (position)
            """
            h = 0
            a = 101
            for c in key:
                h = (h * a + ord(c)) % self.table_size
            return h

        def __len__(self):
            """
            Dunder method to return length of HashTable
            :complexity: O(1) due to return
            :raises: None
            :return: Length of the HashTable
            """
            return self.count

        def __str__(self):
            """
            Dunder method to print HashTable
            :complexity: O(N), where N is the size of the table
            :return res: Result(s) in the HashTable
            """
            res = ""
            for i in range(self.table_size):
                if self.array[i] is not None:
                    res = res + "(" + str(self.array[i][0]) + "," + str(self.array[i][1]) + ")\n"
            return res

        def __getitem__(self, key):
            """
            Dunder method to get item in HashTable
            :complexity: O(1), When the item is already at hash position, O(N) when hash table is full and key doesn not exist.
            :param key:
            :raises KeyError: When there is no value in the position in HashTable
            :return value: If value is present, return value based on key
            """
            hash_value = self.hash(key)

            for _ in range(self.table_size):
                if self.array[hash_value] is None:
                    raise KeyError("Key doesn't exists")
                elif self.array[hash_value][0] == key:
                    return self.array[hash_value][1]
                else:
                    hash_value = (hash_value + 1) % self.table_size
            raise KeyError("Key doesn't exists")

        def __setitem__(self, key, value):
            """
            Dunder method to set value in HashTable based on key value
            :complexity: O(1), When key location is empty, O(N) when table is full and key doesn't exist
            :param key: Key to insert in HashTable
            :param value: Value to store in the key position
            :raises Exception: After looping table and finds that table is full, unable to insert key
            :return none:
            """
            hash_value = self.hash(key)

            for _ in range(self.table_size):
                if self.array[hash_value] is None:
                    self.array[hash_value] = (key, value)
                    self.count += 1
                    return
                elif self.array[hash_value][0] == key:
                    self.array[hash_value] = (key, value)
                    return
                else:
                    hash_value = (hash_value + 1) % self.table_size
            raise Exception("Table is full! & key does not exist in table yet.")

        def __contains__(self, key):
            """
            Dunder methods that return true if key exists in HashTable and vice versa
            :complexity: O(1), when item is at key position, O(N) when table is full and key is not in
            :param key: User's input key
            :raises: None
            :return: Either True or False
            """
            hash_value = self.hash(key)

            for _ in range(self.table_size):
                if self.array[hash_value] is None:
                    return False                    # Return False if no value is present in location
                elif self.array[hash_value][0] == key:
                    return True                     # Return True if value is similar to key at location
                else:
                    hash_value = (hash_value + 1) % self.table_size
            return False                            # Return False nothing is found after going through one loop


# Don't fret! Just testing out my hashTable here
if __name__ == "__main__":
    ht_one = HashTableLinearProbing(5)
    ht_one["Hello"] = "Bye"
    ht_one["Me"] = "You"
    ht_one["Strawberry"] = "Jam"
    ht_one["Keyboard"] = "Mouse"
    ht_one["Dog"] = "Cat"
    print(ht_one)
    print(ht_one.hash("Me"))
    print(ht_one["Me"])
    ht_one["Strawberry"] = "Raspberry"
    print(len(ht_one))

    ht_two = HashTableLinearProbing(7)
    ht_two["Love"] = "Hate"
    ht_two["Real"] = "Artificial"
    ht_two["Truth"] = "Dishonesty"
    ht_two["Bitter"] = "Sweet"
    ht_two["Argument"] = "Reasoning"
    ht_two["Table"] = "Chair"
    ht_two["Speak"] = "Listen"
    print(ht_two)
    print(ht_two.hash("Bitter"))
    print(ht_two["Truth"])

    ht_three = HashTableLinearProbing(10)
    ht_three["Completed"] = "Start"
    ht_three["Coffee"] = "Tea"
    ht_three["Threat"] = "Guard"
    ht_three["Why"] = "Solution"
    ht_three["Red"] = "Blue"
    ht_three["Against"] = "Collaborate"
    ht_three["Pots"] = "Pan"
    ht_three["Pencil"] = "Eraser"
    ht_three["Tree"] = "Paper"
    ht_three["Sea"] = "Land"
    print(ht_three)
    print(ht_three.hash("Coffee"))
    print(ht_three["Tree"])

