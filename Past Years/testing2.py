# 2017 (i)
# 1
# 80 bits

#2
# 32 bits

#3
# Yes recursive is no different from iterative, instead of storing past results and revisit
# we just use a variable in iterative to store the result

#4
# Quick sort is not stable, if I choose the middle element to partition B1 and
# the same element in the front B2 and swap them, therefore making it not stable

#5
# Register that contains the memory address of the next instruction

#6
# best time complexity: when item is smallest in the list
# worst time complexity: when item is largest in the list

#7
# Circular Queue
# Priority Queue
# Array based Queue

#8
# When result is stored in function parameter and no computation is done on the way out

#9
# [1, 2, 3, 4]

#10
# self.item
# self.left pointing left of the subtree
# self.right pointing right of the subtree

# 2(a)

def resize(self):
    new_array = (len(self.array)*2) * [None]
    for i in range(len(self.array)):
        new_array[i] = self.array[i]
    self.array = new_array

# 2(b)
# Worst Case: O(N) when the array needs to resize
# Best Case: O(1) just push the item into the stack

# 2(c)
# N/A

# 3(a)
def __len__(self):
    return self.len_aux(self.root)

def len_aux(self, current):
    if current is None:
        return 0
    else:
        return len_aux(current.left) + 1 + len_aux(current.right)

# 3(b)
def is_balanced(self):
    return self.balanced_aux(self.root)

def balanced_aux(self, current):
    if current is not None:
        return True
    else:
        dif = self.height_aux(current.left) - self.height_aux(current.right)
        if abs(dif) > 1:
            return False
        else:
            return self.balanced_aux(current.left) and self.balanced_aux(current.right)

# 3(c)
# root, left, right
# O(N) since all nodes are processed exactly once

# 6(a)
# Yes, __init__ and shift belong to the same namespace as they are under the same class.

# 6(b)
# No, they belong under their own respective method, __init__ and shift

# 6(c)
# It is bound in line 6, when the method is called with particular values for all its arguments

# 7(a)

def periodic_delete(self, n):
    self.head = self.find(self.head, n)
    current = self.head

    while current is not None:
        last_keep = self.find(current, n-1)
        if last_keep is not None:
            current = self.find(last_keep.link, n)
            last_keep.link = current
        else:
            current = None

# 8(a)
# During partition phase in mergesort, it doesn't matter what the value of elements is ,I just have to
# partition it down to a single element each which is technically sorted.

# 8(b)
# It will only reduce the size of the problem by one, as it is a very huge number and all numbers in the list
# will be smaller than that.

# 9(a)
# Worst case complexity of heap sort (n log n)
# add, get max since they only reduce the problem by half, each item needs to rise or sink to the most top or bottom
# of the list.

# 9(c)
# 13, 8, 6, 7, 1, 3
# 3, 8, 6, 7, 1, 13
# 8, 3, 6, 7, 1, 13
# 8, 7, 6, 3, 1, 13

# 10(a)
# Binary Search Tree O(log N) to tranverse list worst will be O(N)
# Array Sorted List O(N) to loop through insert and delete
# Hash Table Constant O(1) but worst O(N) insert delete are O(N)










def insert(self, key, item):
    position = self.hash(key)
    for _ in range(len(self.array)):
        if self.array[position] is None:
            self.array[position] = (key, item)
            self.count += 1
            return
        elif self.array[position][0] == key:
            self.array[position] = (key, item)
            return
        else:
            position = (position + 1) % self.table_size

def hash(self, word):
    val = 0
    for i in range(len(word)):
        val = (val * 31 + ord(word[i])) % self.table_size
    return val

def __getitem__(self, key):
    position = hash(key)
    for _ in range(self.table_size):
        if self.array[position] is None:
            raise KeyError("Not present")

        elif self.array[position][0] == key:
            return self.array[position][1]

        else:
            position = (position + 1) % self.table_size
    raise KeyError("Not present")

