def length(self):
    return self.length_aux(self.root)


def length_aux(self, current):
    if current is None:
        return 0
    else:
        return length_aux(current.left) + 1 + length_aux(current.right)

# Quick Sort Question
# since I have a magic partition algorithm that is O(1)
# my best case will be O(log n) depending on the choice of pivot (median)
# my worst case will be O(n) when the problem is reduced by only one each iteration (smallest or largest pivot)
# At each iteration, selection sort finds the smallest element

# 2016 (ii)
# 4(a)


def push(self, item):
    assert not is_full(), "Stack is full"
    if self.top is None:
        self.array[0] = item
        self.count += 1
        self.top = self.array[0]

    self.array[self.top] = item
    self.count += 1
    self.top += 1

# 4(b)


def pop(self):
    assert not is_empty(), "Stack is empty"
    item = self.array[self.top]
    self.top -= 1
    self.count -= 1
    if self.array[self.top] is None:
        self.top = 0
    return item

# 5(a)


def add(self, item):
    current = self.head
    if self.head is None:
        self.head = Node(item, None)
        return
    while current < len(a_list):
        if item > current.item:
            current = current.next
    current.next = Node(item, current.next)
    self.count += 1

# 5(b)

"""
best case O(1) when the list is empty
worst case O(n) need to find correct position to insert element
"""

# 5(c)


def __next__(self):
    if self.current is None:
        raise StopIteration
    else:
        item = self.current.item
        self.current = self.current.next
        return item


def __next2__(self):
    if self.current_index >= len(self):
        raise StopIteration

    else:
        item = self.array[self.index]
        self.index += 1
        return item

# 7(b)

"""
root, left, right
pre: +, -, *, a, b, /, c, d, e, *, e, f
"""

# 7(c)
"""
left, right, root
post: a, b, *, c, d, /, -, e, f, *, +
"""

# 7(d)


def insert_aux(self, current, key, item):
    if current is None:
        current = TreeNode(key, item)
    elif key < current.key:
        self.insert_aux(current.left, key, item)
    elif key > current.key:
        self.insert_aux(current.right, key, item)
    else:
        raise ValueError("Duplicate item")
    return current


# 8(a)


def rise(self, k):
    while k > 1 and self.array[k] > self.array[k//2]:
        self.swap(k, k//2)
        k //= 2


#### additional ###

def add(self, key, value):
    item = (key, value)
    if self.count + 1 < len(self.array):
        self.array[self.count + 1] = item
    else:
        self.resize()
        self.array[self.count + 1] = item
    self.count += 1
    self.rise(self.count)


def largest_child(self, k):
    if 2*k == self.count or 2*k > 2*k+1:
        return 2*k
    else:
        return 2*k+1


def sink(self, k):
    while 2*k <= len(self.array):
        child = self.largest_child(k)
    if self.array[k] >= self.array[2*k]:
        break
    self.swap(k, 2*k)
    k = child

def get_max(self):
    tbr = self.array[1][0]
    self.swap(1, self.count)
    self.count -= 1
    self.sink(1)
    return tbr

# 8(b)


def largest_child(self, k):
    if 2*k == self.count or 2*k > 2*k+1:
        return 2*k
    else:
        return 2*k+1


# 8(c)


def get_minimum(a_max_heap):




# 9(a)
"""
Quadratic probing sets key as h + S^2 during collision (s being the step)
After a collision, it will look in position h + 1^2, 2^2
"""

# 9(b)
"""
During collision in separate chaining, add a linked list at the hash position.
If collision, add element to the list.

"""

# 9(c)
"""
No, because with a perfect hash, there will be no collision and will assign
different position.
"""

### Testing ###
def __len__(self):
    return len_aux(self, self.root)

def len_aux(self, current):
    if current is None:
        return 0
    else:
        return self.len_aux(current.left) + 1 + self.len_aux(current.right)

def get_leaves(self):
    a_list = []
    return get_leaves_aux(self.root, a_list)
    return a_list

def is_leaves(self, current):
    if current.left is None and current.right is None:
        return True
    return False

def get_leaves_aux(self, current, a_list):
    if current is not None:
        if self.is_leaves(current):
            a_list.append(current.item)
    else:
        self.get_leaves_aux(current.left, a_list)
        self.get_leaves_aux(current.right, a_list)
