			.data
n:			.word	0
str1:			.asciiz	"Enter integer: "			
			.text
print:			li	$v0, 4
			la	$a0, str1
			syscall
			
			li	$v0, 5
			syscall
			sw	$v0, n
			
loop:			li	$t0, 1
			lw	$t1, n
			ble	$t1, $t0, endLoop
			
			li	$v0, 1			
			lw	$a0, n
			syscall
			
if:			lw	$t0, n
			li	$t1, 2
			div 	$t0, $t1
			mfhi	$t2
			beq	$t2, $zero, else
			
			li	$t0 ,3
			lw	$t1, n
			li	$t2, 1
			mult	$t0, $t1
			mflo	$t3
			add	$t4, $t3, $t2
			sw	$t4, n
			j	loop
			
else:			lw	$t0, n
			li	$t1, 2
			div	$t0, $t1
			mflo	$t2
			sw	$t2, n
			j 	loop
			
endLoop:		li	$v0, 1
			lw	$a0, n
			syscall
			
			li	$v0, 10
			syscall
			
					
			