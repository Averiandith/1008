		.data
a:		.word	0
b:		.word	0
c:		.word	0	
		.text
		
		li	$v0, 5
		syscall
		sw	$v0, a
		
		li	$v0, 5
		syscall
		sw	$v0, b
		
		# Caller part
		addi	$sp, $sp, -8	# Passes arguments on stack
		lw	$t0, a
		sw	$t0, ($sp)
		lw	$t0, b
		sw	$t0, 4($sp)
		
		jal	remainder	# Call function using jal remainder
		
		# Return (Caller)
		addi	$sp, $sp, 8	# Clear arguments off stack
		
		sw	$v0, c		# Uses return value in $v0
		li	$v0, 1
		lw	$a0, c
		syscall
		
		li	$v0, 10
		syscall
		
		
remainder:	# Callee
		addi	$sp, $sp, -8	# Saves $ra and $fp on stack
		sw	$ra, 4($sp)
		sw	$fp, ($sp)
		
		add 	$fp, $sp, $zero	# Copies $sp to $fp
		
		addi	$sp, $sp, -4	# Allocates local variables on stack
		
		lw	$t0, 8($fp)
		lw	$t1, 12($fp)
		div	$t0, $t1
		mfhi	$t0
		sw	$t0, -4($fp)
		
		# Return (Callee)
		
		lw	$v0, -4($fp)	# Sets $v0 to return value
		
		addi	$sp, $sp, 4	# Clear local variables off stack
		
		lw	$fp, ($fp)	# Restores saved $fp and $ra off stack
		lw	$ra, 4($sp)	
		
		addi	$sp, $sp, 8	# Move it down, do not need $sp and $fp anymore
		
		jr	$ra
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
