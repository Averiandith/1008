	.data
str1:	.asciiz	"Enter size: "
str2:	.asciiz	"Your list is: "
n:	.asciiz	"\n"
size:	.word	0	# Size of the list
i:	.word	0
list:	.word	0	# Add of first byte here
	.text
	
	li	$v0, 4		# Print str "Enter Size"
	la	$a0, str1
	syscall
	
	li	$v0, 5		# Request user's input (size)
	syscall
	sw	$v0, size
	
	lw	$t0, size	# size * 4 + 4 
	li	$t1, 4
	mult	$t0, $t1
	mflo	$t2
	add	$a0, $t2, $t1
	li	$v0, 9
	syscall
	
	sw	$v0, list	# Store add of first byte to list
	sw	$t0, ($v0)	# Store the size into add of first byte in list
	
	sw	$zero, i
	
loop1:	lw	$t0, i		# Check if i < 0
	lw	$t1, size
	slt	$t0, $t0, $t1
	beq	$t0, $zero, endL	# Go loop2 if i < 0
	
	li	$v0, 5		# Request user's input
	syscall
	
	lw	$t0, i		# Load i to $t0
	li	$t1, 4
	mult	$t0, $t1
	mflo	$t2
	add	$t3, $t2, $t1	# (size * 4) + 4
	
	lw	$t4, list
	add	$t4, $t4, $t3
	sw	$v0, ($t4)	# Store user's input into add shown in $t4
	
	lw	$t0, i		# Load $t0 to i
	addi	$t0, $t0, 1	# Increment by 1
	sw	$t0, i		# Store it
	j 	loop1
	
endL:	sw	$zero, i

	li	$v0, 4
	la	$a0, n
	syscall
	
	li	$v0, 4
	la	$a0, str2
	syscall
	
	li	$v0, 4
	la	$a0, n
	syscall
	
loop2:	lw	$t0, i		
	lw	$t1, size
	slt	$t0, $t0, $t1
	beq	$t0, $zero, endL2
	
	lw	$t0, i			# size * 4 + 4
	li	$t1, 4
	mult	$t0, $t1
	mflo	$t2
	add	$t3, $t2, $t1
	
	lw	$t4, list		# load add of 1st byte in $t4
	add	$t4, $t4, $t3
	
	lw	$a0, ($t4)		# load (item in add in $t4)
	li	$v0, 1
	syscall
	
	li	$v0, 4
	la	$a0, n
	syscall
	
	lw	$t0, i			# Increment i += 1
	addi	$t0, $t0, 1
	sw	$t0, i
	j	loop2
	
endL2:	li	$v0, 10
	syscall
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
