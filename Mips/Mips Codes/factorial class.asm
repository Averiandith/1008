	.data
str1:	.asciiz	"Enter a number: "
f:	.word	0
n:	.word	0
	.text
	
	li	$v0, 4
	la	$a0, str1
	syscall
	
	li	$v0, 5
	syscall
	sw	$v0, n

	li	$t0, 1
	sw	$t0, f
	
	# While loop
whileL:	lw	$t0, n
	slt	$t1, $zero, $t0
	beq	$t1, $zero, endL
	
	lw	$t0, f
	lw	$t1, n
	mult	$t0, $t1
	mflo	$t0
	sw	$t0, f
	
	lw	$t0, n
	addi	$t0, $t0, -1
	sw	$t0, n
	j 	whileL
	
endL:	li	$v0, 1
	lw	$a0, f
	syscall
	li	$v0, 10
	syscall
	
	
	