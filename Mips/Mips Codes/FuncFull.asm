		.data
a:		.word 1
		.text
		
		addi $sp, $sp, -4	# argument
		
		lw $t0, a
		sw $t0, ($sp)
		
		jal increment		# caller
		
		add $sp, $sp, 4		# (ret) clear argument off stack
		
		add $a0, $v0, $0	# Uses return value in ($v0)
		li $v0, 1		# Print value
		syscall
		
		li $v0, 10
		syscall
		
			
increment:	# callee
		addi $sp, $sp, -8	# register ($ra, $fp)
		sw $ra, ($sp)
		sw $fp, 4($sp)
		
		add $fp, $sp, $0	# point $sp and $fp to the same location
		
		addi $sp, $sp, -4	# create a space for local variable
		
		lw $t0, 8($fp)		# y = argument + 1
		addi $t0, $t0, 1
		sw $t0, -4($fp)
		
		lw $v0, -4($fp)		# sets $v0 to return value
		
		addi $sp, $sp, 4	# clear local variables off stack
		
		lw $ra, ($fp)		# restores saved $fp and $ra off stack
		lw $fp, 4($fp)
		
		addi $sp, $sp, 8	# returns to caller with jr $ra
		
		jr $ra
		
		
		
