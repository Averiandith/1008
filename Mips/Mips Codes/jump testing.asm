	.data
str1:	.asciiz	"What is your int? "
enter:	.asciiz	"\n"
x:	.word	0
y:	.word	0
z:	.word	0
	.text
	
	li	$v0, 4
	la	$a0, str1
	syscall
	
	li	$v0, 5
	syscall
	sw	$v0, x
	
	li	$v0, 4
	la	$a0, str1
	syscall
	
	li	$v0, 5
	syscall
	sw	$v0, y
	
mult:	lw	$t0, x
	lw	$t1, y
	mult	$t0, $t1
	mflo	$t0
	sw	$t0, z
	jal	add	# initializing jumping here
	li	$v0, 4
	la	$a0, enter
	syscall
	li	$v0, 1
	lw	$a0, z
	syscall
	j	end
	
add:	li	$v0, 1	# subroutine begins here
	lw	$a0, z
	syscall
	lw	$t0, x
	lw	$t1, y
	add	$t0, $t0, $t1
	sw	$t0, z
	jr	$ra
	
end:	li	$v0, 10
	syscall