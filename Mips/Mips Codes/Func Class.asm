	.data
m:	.word	2
n:	.word	3
	.text
	
	# Keep sp as the pointer to stack
	# Use fp to access item in the stack
	
	add	$fp, $sp, $zero	# Move $sp into $fp
	addi	$sp, $sp, -8	# Moving 2 slots
	lw	$t0, n
	addi	$t0, $t0, 2	# n + 2
	sw	$t0, -8($fp)	# store m here
	
	lw	$t0, -8($fp)	# sp is now in m
	li	$t1, 2
	mult	$t0, $t1
	mflo	$t0
	sw	$t0, -4($fp)	# +4 to $sp, point below m, which is a
	
	li	$v0, 1
	lw	$a0, -8($fp)	
	syscall
		
	li	$v0, 1
	lw	$a0, -4($fp)
	syscall
	
	li	$v0, 10
	syscall
	
	# jr $ra (the only jump register inst we are going to use)
	
	
