	.data
prompt:	.asciiz	"What is your number? "
f:	.word	1
n:	.word	0
	.text
	# print prompt
	addi	$v0, $zero, 4
	la	$a0, prompt
	syscall
	
	# read n
	addi	$v0, $zero, 5
	syscall
	sw	$v0, n
	
	# if n <= 0, goto end loop
loop:	lw	$t0, n
	slt	$t1, $t0, $zero
	beq	$t1, $zero, endL # if it's equal, jump to endloop
	mult	$t1, $t0
	mflo	$t1
	sw	$t1, f
	# n -= 1
	lw	$t0, n 
	addi	$t0, $t0, -1
	sw	$t0, n 
	j loop

endL:	addi	$v0, $zero, 1
	lw	$a0, f
	syscall
	addi	$v0 ,$zero, 10
	syscall