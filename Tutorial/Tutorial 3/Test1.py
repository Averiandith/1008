def least_vowel_words(text):
    text = text.lower()
    text = text.split()

    minVowel = vowel_proportion(text[0])
    minVowelWords = [text[0]]

    for word in text:
        word = word.strip(",")
        word = word.strip(".")
        word = word.strip("?")
        word = word.strip("'")
        word = word.strip("!")
        word = word.strip("/")
        if vowel_proportion(word) < minVowel:
            minVowelWords = [word]
            minVowel = vowel_proportion(word)
        elif vowel_proportion(word) == minVowel:
            minVowelWords.append(word)
    return minVowelWords

def vowel_proportion(word):
    ctr = 0
    for i in word:
        if i in ['a','e','i','o','u']:
            ctr += 1
    return ctr/len(word)

print(least_vowel_words("the quality of mercy is percy"))