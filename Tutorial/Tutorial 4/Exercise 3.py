"""def odd_product(aList):
    product = 1
    for x in aList:
        if x % 2 != 0:
            product = product * x
    return product"""

# Faithful translation

def odd_product(aList):
    size = len(aList)
    product = 1
    i = 0
    while i < size:
        x = aList[i]
        if x % 2 != 0:
            product = product * x
        i += 1
    return product


aList = [1, 2, 3, 4, 5]
print(odd_product(aList))
