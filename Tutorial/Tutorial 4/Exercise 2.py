def collatz(n):

    # Draw a stack diagram at this time

    if n % 2 == 0:
        return n / 2
    else:
        return 3 * n + 1

n = int(input("Enter integer: "))

while n > 1:
    print(n)
    n = collatz(n)
