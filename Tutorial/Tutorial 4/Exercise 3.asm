		.data
aList:		.word	0
size:		.word	0

		.text
		
		# Initialize the list
		li	$t0, 5
		sw	$t0, size	# Assume size of list is 5
		li	$t1, 4
		mult	$t0, $t1	# Size * 4 + 4
		mflo	$t2
		add	$a0, $t2, $t1
		li	$v0, 9
		syscall
		
		sw	$v0, aList
		sw	$t0, ($v0)
		
		
		# Function (Caller)
		addi	$sp, $sp, -4
		lw	$t0, list
		sw	$t0, ($sp)
		
		jal	odd_product
		
		addi 	$sp, $sp, -8
		sw	$ra, 4($sp)
		sw	$fp, ($sp)
		
		addi	$fp, $sp, $zero
		
		addi	$sp, $sp, -16
		
		
		