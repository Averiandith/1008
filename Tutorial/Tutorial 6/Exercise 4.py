class CoffeeShop:

    # Maintains a list of customers and a list of coffees

    def __init__(self, ID_list, coffee_list):
        self.ID_list = ID_list
        self.coffee_list = coffee_list

class Customer:

    # Contains customer attributes and methods, attributes include name, phone num and number of points
    # The methods might include one that updates the number of points, each time a coffee is bought

class Coffee:

    # Contains coffee attributes and methods. Attributes include type, price and day of the week in which the coffee is discounted

class List:

    # attributes of the_array and length and methods empty, is_full, add, delete...1