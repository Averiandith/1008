class PoolMember:
    pool_name = "FIT1008 students community pool"
    name = ""
    age = 0
    gender = None

    def __init__(self, name, age, gender):
        PoolMember.name = name
        PoolMember.age = age
        PoolMember.gender = gender


admin = PoolMember("Juan", 18, "male")
admin.pool_name = "MONASH all-inclusive community pool"
print(admin.pool_name)
print(admin.name)
print(admin.age)
print(admin.gender)
print(admin.pool_name)

supervisor = PoolMember("Emilia", 26, "female")
print(supervisor.pool_name)

print(admin.name)
print(admin.age)
print(admin.gender)

