class PoolMember:
    pool_name = "FIT1008 students community pool"
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


admin = PoolMember("Juan", 18, "male")
PoolMember.pool_name = "MONASH all inclusive community pool"

print(admin.pool_name)

supervisor = PoolMember("Emilia", 26, "female")
print(supervisor.pool_name)

print(admin.name)
print(admin.gender)
print(supervisor.name)
print(supervisor.gender)



