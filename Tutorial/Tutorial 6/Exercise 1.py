def silly():
    x = "ping"      # Here x is "ping"

    def g():        # g is defined here
        print(x)    # It's true that this x means the enclosing scope's x
    x = "pong"      # But here we change the enclosing scope's x to mean "pong"

    def f(x):
        print(x)
    g()             # So by the time we call g(), the enclosing scope's x
    f(x)            # Is the same than when we call f(x), that is, "pong"


silly()             # Therefore, this will print "pong" twice, on two separate lines

