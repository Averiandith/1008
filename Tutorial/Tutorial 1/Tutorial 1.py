#Exercise 1

aList = [1, 2, 3, 4, 5, 6, 7]
count = 0
sum = 0
i = 0

while i < len(aList):
    print("Outside")
    count += 1
    sum += aList[i]
    j = 0

    while j < len(aList):
        print("     Inside")
        count += 1
        sum += aList[j]
        j += 1
    i += 1

print("Sum: " + str(sum))
print(count)

"""(i) For a list of length 7, how many times does the mystery function print "Outside" and print "     Inside"?
"Outside" = 7
"     Inside" = 49

(ii) What is the value returned in count for a list of length 7?
count = 56

(iii) What value is printed for sum when the list contains numbers 1,2,3,4,5,6 and 7?
sum = 224"""


# Exercise 2

def reversed(aWord):
    var = aWord
    reversed = aWord[::-1]
    if var == reversed:
        return True
    return False

aWord = input("\nWhat is your word? ")
if reversed(aWord) == True:
    print("Word is a palindrome\n")
else:
    print("Word is not a palindrome\n")


# Exercise 3

list1 = ["apple", "orange", "bag"]
list2 = ["milk", "egg", "apple", "orange"]

def printSameItem(list1, list2):
    for item1 in list1:
        for item2 in list2:
            if item1 == item2:
                print(item1)

printSameItem(list1, list2)


# Exercise 4

"""(i) How does a heap differ from a binary search tree? 
Min-Heap: Root node smallest, Each parent is always smaller or equal to it's children.
BST: Each node of left subtree is less that it's value, each node of right subtree is greater than it's value.

(ii) How does a stack differ from a queue?
Stack: Last in first out (interact with item at top)
Queue: First in first out (interact with item at front and rear)

(iii) How does recursion differ from iteration?
Recursion: A function repeatedly calling itself, turning the problem to a smaller instance each call till it reaches the base case where the problem will be solved.
Iteration: A function that loops to repeat some section of the code.
"""

