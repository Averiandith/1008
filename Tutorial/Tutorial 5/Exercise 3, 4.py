# Exercise 3
a = [0, 1]

try:
    b = a[2]
    print("That worked")

except ValueError:
    print("No it didn't!")

except IndexError:
    print("Index is wrong")

# Exercise 4
a = [0, 1]

try:
    b = a[2]
    c = int("foo")
    d = e
    f = 1/0
    print(1 + "1")

except:
    print("What happened")

# Exercise 5

def divide(a, b):
    return a / b

def test_divide():
    try:
        divide(1, 0)
        raise Exception("ZeroDivisionError not raised.")
    except ZeroDivisionError:
        pass

test_divide()
print("All test case passed")
