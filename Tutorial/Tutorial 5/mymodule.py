class player:
    # dunder methods/ magic methods
    def __init__(self):
        self.hp = 10

    def take_damage(self, damage):
        self.hp -= damage
