class LinkedList:
    def __init__(self):
        self.head = None
        self.count = 0

    def delete_negative(self):
        while self.head is not None and self.head.item < 0:
            self.head = self.head.next

        if self.head != None:
            previous = self.head
            current = self.head.next
            while current is not None:
                if current.item < 0:
                    previous.next = current.next
                else:
                    previous = current
            current = current.next



