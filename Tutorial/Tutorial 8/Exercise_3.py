class CircularQueueIterator:
    def __init__(self):
        self.array = queue.array
        self.count = 0
        self.size_of_q = queue.count
        self.current = queue.front

    def __iter__(self):
        return self

    def __next__(self):
        if self.count >= self.size_of_q:
            raise StopIteration
        item = self.array[self.current]
        self.current = (self.current + 1) % len(self.array)
        self.count += 1
        return item


