from referential_array import build_array


class SortedListADT:
    def __init__(self, size):
        self.count = 0
        self.array = build_array(size)

    def index(self, item):
        low = 0
        high = len(self) - 1

        while low <= high:
            mid = (low + high) // 2
            if item == self.array[mid]:
                if low == high:
                    return low
                high = mid
            elif item < self.array[mid]:
                high = mid - 1
            else:
                low = mid + 1
        raise ValueError("Item not in list")



        raise ValueError("Item is not in list")