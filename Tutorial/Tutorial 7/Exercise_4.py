class CircularQueue:
    def __init__(self, size):
        assert size > 0, "Size must be > 0"
        self.array = [None] * size
        self.front = None
        self.rear = None
        self.reset()

    def reset(self):
        self.front = 0
        self.rear = 0
        self.count = 0

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.count >= len(self.array)

    def serve(self):
        assert not self.is_empty(), "Queue is empty"
        item = self.array[self.front]
        self.front = (self.front + 1) % len(self.array)
        self.count -= 1
        return item

    def append(self, item):
        assert not self.is_full(), "Queue is full"
        self.array[self.rear] = item
        self.rear = (self.rear + 1) % len(self.array)
        self.count += 1
