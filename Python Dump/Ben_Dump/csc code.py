def check_csc(code):
	n = len(str(code))
	if n == 3:
		while True:
			try:
				code = int(code)
				return True
			except ValueError:
				return False
	return False

def check_csc2(code):
	n = len(str(code))
	numList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
	if n == 3:
		for i in code:
			isNumeric = False
			for number in numList:
				if i == number:
					isNumeric = True
		return isNumeric
	return False



print(check_csc2("aaa"))
print(check_csc2("333"))
print(check_csc2("a3d"))