def checkIsNumeric(item):
    intList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
    for i in item:
        isNum = False
        for int in intList:
            if i == int:
                isNum = True
        return isNum
    return False

num = input("Enter the number for 'num': ")
N = input("Enter the number for 'N': ")

if checkIsNumeric(num) == True and checkIsNumeric(N) == True:
    N = int(N)
    num = int(num)
    for i in range(1, N + 1):
        result = i * N
        print(str(i), "*", str(num), "=", str(result))
else:
    print("Invalid input")

