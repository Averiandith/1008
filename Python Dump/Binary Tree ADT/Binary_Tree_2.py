from tree_node import TreeNode


class PreOrderIterator(self):
    def __init__(self, root):
        self.current = root
        self.stack = []
        self.stack.append(root)

    def __iter__(self):
        return self

    def __next__(self):
        if self.stack == []:
            raise StopIteration
        current = self.stack.pop()
        if current.right is not None:
            self.stack.append(current.right)
        if current.left is not None:
            self.stack.append(current.left)
        return current.item


class BinaryTree:
    def __init__(self):
        self.root = None

    def __iter__(self):
        return PreOrderIterator(self)

    def __len__(self):
        return self.len_aux(self.root)

    def len_aux(self, current):
        if current is None:
            return 0
        else:
            return len_aux(current.left) + 1 + len_aux(current.right)

    def get_leaves(self):
        a_list = []
        self.get_leaves_aux(self.root, a_list)
        return a_list

    def is_leaves(self, current):
        return current.left is None and current.right is None

    def get_leaves_aux(self, current, a_list):
        if current is not None:
            if self.is_leaves(current):
                 a_list.append(current.item)
             else:
                 self.get_leaves_aux(current.left, a_list)
                 self.get_leaves_aux(current.right, a_list)

    def insert(self, item, bit_string):
        bit_string_iter = iter(bit_string)
        self.root = self.insert_aux(self.root, item, bit_string_iter)

    def insert_aux(self, current, item, bit_string_iter):
        if current is None:
            current = TreeNode(None, None, None)
        try:
            bit = next(bit_string_iter)
            if bit == "0":
                current.left = self.insert_aux(current.left, item, bit_string_iter)
            elif bit == "1":
                current.right = self.insert_aux(current.right, item, bit_string_iter)
        except StopIteration:
            current.item = item
        return current

    def print_pre_order(self):
        self.print_pre_order_aux(self.root)

    def print_pre_order_aux(self, current):
        if current is not None:
            print(current.item)
            self.print_pre_order_aux(current.left)
            self.print_pre_order_aux(current.right)

    def print_in_order(self):
        self.print_in_order_aux(self.root)

    def print_in_order_aux(self, current):
        if current is not None:
            self.print_in_order_aux(current.left)
            print(current.item)
            self.print_in_order_aux(current.right)

    def print_post_order(self):
        self.print_post_order_aux(self.root)

    def print_post_order_aux(self, current):
        if current is not None:
            self.print_post_order_aux(current.left)
            self.print_post_order_aux(current.right)
            print(current.item)




