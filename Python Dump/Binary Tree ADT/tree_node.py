class TreeNode:
    def __init__(self, item, left, right):
        self.item = item
        self.left = left
        self.right = right


class BSTNode:
    def __init__(self, key, value, left, right):
        self.key = key
        self.value = value
        self.left = left
        self.right = right
