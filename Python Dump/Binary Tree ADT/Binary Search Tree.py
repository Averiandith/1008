from tree_node import BSTNode


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def __setitem__(self, key, value):
        self.root = self.setitem_aux(self.root, key, value)

    def setitem_aux(self, current, key, value):
        if current is None:
            current = BSTNode(key, value, None, None)
        elif key < current.key:
            self.setitem_aux(current.left, key, value)
        elif key > current.key:
            self.setitem_aux(current.right, key, value)
        else:
            current.value = value
        return current

    def get_item(self, key):
        return self.get_item_aux(self.root, key)

    def get_item_aux(self, current, key):
        if current is None:
            raise KeyError("Key not found")
        elif key < current.key:
            self.get_item_aux(current.left, key)
        elif key > current.key:
            self.get_item_aux(current.right, key)
        else:
            return current.value







