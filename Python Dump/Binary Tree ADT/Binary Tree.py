from tree_node import TreeNode


class BinaryTree:
    def __init__(self):
        self.root = None

    def is_empty(self):
        return self.root is None

    def insert(self, item, bitstring):
        bitstring_iter = iter(bitstring)
        self.root = self.insert_aux(self.root, item, bitstring_iter)

    def insert_aux(self, current, item, bitstring_iter):
        if current is None:
            current = TreeNode(None, None, None)

        try:
            bit = next(bitstring_iter)
            if bit == "0":
                current.left = self.insert_aux(current.left, item, bitstring_iter)
            elif bit == "1":
                current.right = self.insert_aux(current.right, item, bitstring_iter)
        except StopIteration:
            current.item = item
        return current

    def print_pre_order(self):
        self.print_pre_order_aux(self.root)

    def print_pre_order_aux(self, current):
        if current is not None:
            print(current.item)
            self.print_pre_order_aux(current.left)
            self.print_pre_order_aux(current.right)

    def print_in_order(self):
        self.print_in_order_aux(self.root)

    def print_in_order_aux(self, current):
        if current is not None:
            self.print_in_order_aux(current.left)
            print(current.item)
            self.print_in_order_aux(current.right)

    def print_post_order(self):
        self.print_post_order_aux(self.root)

    def print_post_order_aux(self, current):
        if current is not None:
            self.print_post_order_aux(current.left)
            self.print_post_order_aux(current.right)
            print(current.item)


BT = BinaryTree()
BT.insert(3, "01")
BT.insert(4, "02")
BT.insert(5, "03")
BT.insert(6, "04")
BT.print_pre_order()
BT.print_in_order()
BT.print_post_order()
