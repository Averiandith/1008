from referential_array import build_array
# Heap implementation using array-based


class Heap:
    def __init__(self):
        self.count = 0
        self.array = build_array(100)

    def add(self, key, value):
        item = (key, value)
        if self.count + 1 < len(self.array):
            self.array[self.count + 1] = item
        else:
            self._resize()
            self.array[self.count + 1] = item
        self.count += 1
        self.rise(self.count)

    def swap(self, i, j):
        self.array[i], self.array[j] = self.array[j], self.array[i]

    def rise(self, k):
        while k > 1 and self.array[k] > self.array[k//2]:
            self.swap(k, k//2)
            k //= 2

    def _resize(self):
        new_array = build_array(len(self.array) * 2)
        for i in range(len(self.array)):
            new_array[i] = self.array[i]
        self.array = new_array

    def largest_child(self, k):
        # if 2*k == self.count means child is already in last position (one child)
        if 2*k == self.count or 2*k > 2*k + 1:
            return 2*k
        else:
            return 2*k + 1

    def sink(self, k):
        while 2*k <= self.count:
            child = self.largest_child(k)
            if self.array[k] >= self.array[child]:
                break
            self.swap(child, k)
            k = child

    def get_max(self):
        to_be_returned = self.array[1][0]
        self.swap(1, self.count)
        self.count -= 1
        self.sink(1)
        return to_be_returned

    def __str__(self):
        res = ""
        for i in range(1, mx.count+1):
            res = res + str(mx.array[i][0]) + ","
        return res


def heap_sort(a_list):
    heap = Heap(len(a_list) + 1)
    for item in a_list:
        heap.add(item, "value")
    temp = []
    for i in range(len(a_list)):
        temp.append(heap.get_max())
    return temp











