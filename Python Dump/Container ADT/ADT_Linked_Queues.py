from node import Node


class MyQueue:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.front = None
        self.rear = None
        self.count = 0

    def is_empty(self):
        return self.front is None

    def is_full(self):
        return False

    def append(self, item):
        if self.is_full():
            raise Exception("Queue is full")

        newNode = Node(item, None)
        if self.is_empty():
            self.front = newNode
        else:
            self.rear.next = newNode
        self.rear = newNode
        self.count += 1

    def serve(self):
        if self.is_empty():
            raise Exception("Queue is empty")

        item = self.front.item
        self.front = self.front.next
        if self.is_empty():
            self.rear = None
        self.count -= 1
        return item

    def __str__(self):
        res = ""
        current = self.front
        while current is not None:
            res += str(current.item) + ","
            current = current.next
        return res

    def __repr__(self):
        res = ""
        current = self.front
        while current is not None:
            res += str(current.item) + ","
            current = current.next
        return res

qu = MyQueue(5)
qu.append(3)
qu.append(1)
qu.append(4)
qu.append(5)
qu.append(6)
print(qu)
print(qu.serve())

def serve_unittest():
    qu = MyQueue(5)
    items = [1, 4, 5, 6]
    for item in items:
        qu.append(item)
    for item in items:
        assert qu.serve() == item, "Test failed!"
    # assert qu.serve() == 1, "test failed"
    # assert qu.serve() ==

