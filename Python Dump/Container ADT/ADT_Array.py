import referential_array

# Testing
aList = list()
print(type(aList))
aList.append(4)
print(aList)
x = 4
print(type(x))

class MyList:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.count = 0
        self.array = referential_array.build_array(size)

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.count >= len(self.array)

    def length(self):
        return self.count

    def __len__(self):
        return self.count

    def append(self, item):
        space_available = not self.is_full()
        if space_available:
            self.array[self.count] = item
            self.count += 1
        return space_available

    def delete(self, index):
        valid = index >= 0 and index < self.count
        if valid:
            for i in range(index, self.count - 1):
                self.array[i] = self.array[i + 1]   # Move items appearing after the deleted item
            self.count -= 1     # Decrement count
        return valid

    def myprint(self):
        for i in range(self.count):
            print(self.array[i], end=",")
        print()

    def __str__(self):
        res = ""
        for i in range(self.count):
            res = res + str(self.array[i]) + ","
        return res

    def __getitem__(self, index):
        if index >= 0 and index < self.count:
            return self.array[index]
        else:
            raise Exception("invalid index")

    def __contains__(self, item):
        for i in range(self.count):
            if self.array[i] == item:
                self.count += 1
                return True
        return False

    def add_sorted(self, item):
        if self.is_empty():
            self.array[self.count] = item
            self.count += 1
            return True

        space_available = not self.is_full()
        if space_available:
            position = 0
            while position < self.count and item > self.array[position]:
                position += 1

            for i in range(self.count -1, position -1, -1):
                self.array[i+1] = self.array[i]
            self.array[position] = item
            self.count += 1
        return space_available

mylist = MyList(5)
mylist.append(5)
mylist.append(6)
mylist.append(1)
mylist.append(2)
mylist.append(3)
mylist.myprint()
print(mylist.length())

mylist.delete(1)
mylist.myprint()

print(mylist)
print(len(mylist))

sortedlist = MyList(5)
sortedlist.add_sorted(5)
sortedlist.add_sorted(6)
sortedlist.add_sorted(1)
sortedlist.add_sorted(2)
sortedlist.add_sorted(9)
print(sortedlist)
print(sortedlist[2])

#print(10 in sortedlist)
pylist = [2, 11, 5, 5]
print(3 in pylist)

















