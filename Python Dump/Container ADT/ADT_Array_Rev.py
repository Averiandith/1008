import referential_array

class MyList:
	def __init__(self, size):
		assert size > 0, "size must be > 0"
		self.count = 0
		self.array = referential_array.build_array(size)

	def is_empty(self):
		return self.count == 0

	def is_full(self):
		return self.count >= len(self.array)

	def length(self):
		return self.count

	def __len__(self):
		return self.count

	def append(self, item):
		space_available = not self.is_full()
		if space_available:
			self.array[self.count] = item
			self.count += 1
		return space_available

	def delete(self, index):
		valid = index >= 0 and index < self.count
		if valid:
			for i in range(1, self.count- 1):
				self.array[i] = self.array[i+1]
			self.count -= 1
		return valid

	def __str__(self):
		res = ""
		for i in range(self.count):
			res = res + str(self.array[i]) + ","
		return res

	def __getitem__(self, index):
		if index >= 0 and index < self.count:
			return self.array[index]
		else:
			raise Exception("invalid index")

	def __contains__(self, item):
		for i in range(self.count):
			if self.array[i] == item:
				return True
		return False

	def add_sorted(self, item):
		if self.is_empty():
			self.array[self.count] = item
			self.count += 1
			return True

		space_available = not self.is_full()
		if space_available:
			position = 0
			while position < self.count and item > self.array[position]:
				position += 1
			for i in range(self.count-1, position -1, -1):
				self.array[i+1] = self.array[i]
			self.array[position] = item
			self.count += 1
		return space_available

mylist = MyList(5)
mylist.append(5)
mylist.append(2)
mylist.append(5)
print(mylist)
print(2 in mylist)