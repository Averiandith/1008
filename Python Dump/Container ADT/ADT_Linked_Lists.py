from node import Node


class MyList:
    def __init__(self, size):
        assert size > 0, "size must be  > 0"
        self.count = 0                      # A counter
        self.head = None                    # Head set to None

    def is_empty(self):
        return self.head is None

    def is_full(self):
        return False                        # It will nvr be full

    def __len__(self):
        return self.count                   # Item count

    def get_node(self, index):
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")

        node = self.head
        counter = 0
        while counter < index:
            node = node.next
            counter += 1
        return node

    def insert(self, index, item):
        if index < 0:
            index = 0
        elif index > len(self):
            index = len(self)

        if index == 0:                                  # Like pushing stack
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(index - 1)
            cur_node.next = Node(item, cur_node.next)
        self.count += 1

    def append(self, item):
        if self.is_empty():
            self.head = Node(item, None)
        else:
            cur_node = self.get_node(len(self) - 1)
            cur_node.next = Node(item, None)
        self.count += 1

    def delete(self, index):
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")
        if self.is_empty():
            raise IndexError("list is empty")

        cur_node = self.get_node(index - 1)
        cur_node.next = cur_node.next.next
        self.count -= 1

    def __str__(self):
        res = ""
        current = self.head
        while current is not None:
            res = res + str(current.item) + ","
            current = current.next
        return res

    def __getitem__(self, index):
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")

        node = self.get_node(index)
        return node.item

    """def __contains__(self, item):
        for i in range(self.count):
            if self.array[i] == item:   # Why?
                return True
            return False"""


mylist = MyList(5)
mylist.append(5)
mylist.append(6)
mylist.append(1)
mylist.append(2)
mylist.insert(2, 85719283740198237)
print(len(mylist))
print(mylist)
print(2 in mylist)      # ??








