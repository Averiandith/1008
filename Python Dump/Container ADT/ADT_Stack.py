import referential_array


class MyStack:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.top = -1
        self.count = 0
        self.array = referential_array.build_array(size)

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.count >= len(self.array)

    def push(self, item):
        if self.is_full():
            raise Exception("Stack is full")
        self.top += 1
        self.array[self.top] = item
        self.count += 1

    def pop(self):
        if self.is_empty():
            raise Exception("Stack is empty")
        item = self.array[self.top]
        self.top -= 1
        self.count -= 1
        return item

    def __str__(self):
        res = ""
        for i in range(self.count):
            res += str(self.array[i]) + ","
        return res


st = MyStack(5)
st.push(3)
st.push(5)
st.push(1)
print(st)
print(st.pop())
print(st)

# Reversing string using stack


def reverse_str(str1):
    size = len(str1)
    stack = MyStack(size)
    for i in range(size):         # Push string into stack
        stack.push(str1[i])
    string = ""                   # Create empty string
    while not stack.is_empty():   # While stack is not empty
        string += stack.pop()     # Pop character from stack to string
    return string


print(reverse_str("averiandith"))


