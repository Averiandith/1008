from node import Node

class MyStack:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.top = None
        self.count = 0

    def is_empty(self):         # There is no top node
        return self.top is None

    def is_full(self):          # Never full
        return False

    def push(self, item):
        if self.is_full():
            raise Exception("Stack is full")

        self.top = Node(item, self.top)
        self.count += 1

    def pop(self):
        if self.is_empty():
            raise ValueError("Stack is empty")

        item = self.top.item
        self.top = self.top.next
        self.count -= 1
        return item

    def __str__(self):
        res = ""
        current = self.top
        while current is not None:
            res += str(current.item) + ","
            current = current.next
        return res



