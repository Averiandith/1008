class RationalADT:
    p = 1
    q = 1
    z = 7

    def __init__(self, p, q):
        self.p = p
        self.q = q

    def setP(self, p):
        self.p = p

    def printValue(self):
        print(self.p, self.q)

rnum1 = RationalADT(2, 3)
rnum2 = RationalADT(5, 6)

print(rnum1.p)
print(rnum2.p)

rnum1.setP(9)

print(rnum1.p)  # Set to 9
print(rnum2.p)

print(rnum1.z)
print(rnum2.z)

RationalADT.z = 11

print(rnum2.z)
print(rnum1.z)

