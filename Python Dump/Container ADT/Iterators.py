# Is it alright for me to imagine that iterator is like get_node?


class ListIteratorLinked:
    def __init__(self, head):
        self.current = head

    def __iter__(self):
        return self

    def __next__(self):
        if self.current is None:        # Current is referencing to the pointed node (am I correct?)
            raise StopIteration("No more items available")
        else:
            item_required = self.current.item
            self.current = self.current.next
            return item_required


class ListIteratorArray:
    def __init__(self, array):
        self.array = array
        self.current_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current_index >= len(self.array):
            raise StopIteration("Index out of range")

        else:                  # Call next to proceed to next index
            item = self.array[self.current_index]
            self.current_index += 1
            return item


def maximum(a_list):
    try:
        it = iter(a_list)
        max_val = next(it)
        for item in it:
            if item > max_val:
                max_val = item
        return max_val
    except StopIteration:
        raise Exception("None left")


