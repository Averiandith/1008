import referential_array

class MyQueue:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.front = 0
        self.rear = 0
        self.count = 0
        self.array = referential_array.build_array(size)

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.rear >= len(self.array)

    def append(self, item):
        if self.is_full():
            raise Exception("Queue is full")

        self.array[self.rear] = item    # Append item
        self.rear += 1                  # Increase rear
        self.count += 1                 # Increase count

    def serve(self):
        if self.is_empty():
            raise Exception("Queue is empty")

        item = self.array[self.front]   # Store index in array to item
        self.front += 1                 # Increase front (interact front to take item)
        self.count -= 1                 # Decrease count
        return item                     # Return item

    def __str__(self):
        res = ""
        for i in range(self.front, self.rear):
            res += str(self.array[i]) + ","
        return res

qu = MyQueue(5)
qu.append(3)
qu.append(1)
qu.append(4)
qu.append(5)
qu.append(6)
print(qu)
print(qu.serve())
print(qu.serve())
print(qu.serve())
print(qu.serve())
print(qu.serve())
print(qu)           # Nothing left here
#qu.append(6)