def sum_items(a_list):
    sum = 0
    for item in a_list:
        sum += item
    return sum
if __name__ == "__main__":      # Think about the atribute and characteristics of the python file.
    a_list = [1,2,3,4]          # "__main__" means that this it is a main program
    print("Program in Name_Example.py")
    print(sum_items(a_list))