import referential_array


class HashTableLinearProbe:
    def __init__(self, size):
        self.array = referential_array.build_array(size)
        self.count = 0
        self.table_size = size

    def hash_function(self, key):
        value = 0
        a = 1921
        for i in range(len(key)):
            value = (value * a + ord(key[i])) % self.table_size
        return value

    def __len__(self):
        return self.count

    def __str__(self):
        res = ""
        for i in range(self.table_size):
            if self.array[i] is not None:
                res = res + "(" + str(self.array[i][0]) + "," + str(self.array[i][1]) + ")\n"
        return res

    def __setitem__(self, key, data):
        position = self.hash_function(key)

        for _ in range(self.table_size):        # Loop through the whole array
            if self.array[position] is None:
                self.array[position] = (key, data)
                self.count += 1
                return
            elif self.array[position][0] == key:
                self.array[position] = (key, data)
                return
            else:
                position = (position + 1) % self.table_size

    def __getitem__(self, key):
        position = self.hash_function(key)

        for _ in range(self.table_size):
            if self.array[position] is None:
                raise Exception("key not found")
            elif self.array[position][0] == key:
                return self.array[position][1]
            else:
                position = (position + 1) % self.table_size


HT = HashTableLinearProbe(15)
HT["kruse"] = "bookname"
HT["ada"] = "hello"
HT["horowiz"] = "world"
print(HT)
# print(HT["kruse2"])
