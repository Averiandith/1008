def longest_positive_dp(a_list):
    memo = [0] * len(a_list)
    if a_list[0] > 0:
        memo[0] = 1
    else:
        memo[0] = 0

    maxi = memo[0]
    for i in range(1, len(a_list)):
        if a_list[i] > 0:
            memo[i] = memo[i - 1] + 1
        else:
            memo[i] = 0
        if maxi < memo[i]:
            maxi = memo[i]
    return maxi


b_list = [1, 2, 4, -1, -2, 4, 2, 5, 2, 3, 0, -12, -1]
print(longest_positive_dp(b_list))


def maximum_subsequence_dp(a_list):
    memo = [0] * len(a_list)
    memo[0] = a_list[0]
    maxi = memo[0]
    for i in range(1, len(a_list)):
        memo[i] = max(a_list[i], a_list[i] + memo[i-1])
        if maxi < memo[i]:
            maxi = memo[i]
    return maxi


c_list = [90, 90, 0, -200, 1, 2, 96, -1, -100, 99, 0, 0, -5, -9, -7, 25]
print(maximum_subsequence_dp(c_list))