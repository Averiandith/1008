aList = [1, 2, 3]

it = iter(aList)
print(type(it))
print(next(it))
print(next(it))
print(next(it))


class Node:
    def __init__(self, item, next):
        self.item = item
        self.next = next


class MyListIterator:
    def __init__(self, mylist):
        self.current = mylist.head

    def __iter__(self):
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration

        item = self.current.item
        self.current = self.current.next
        return item


class MyList:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.count = 0
        self.head = None

    def is_empty(self):
        return self.head is None

    def is_full(self):
        return False

    def __len__(self):
        return self.count

    def __iter__(self):
        return MyListIterator(self)

    def get_node(self, index):
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")

        node = self.head
        counter = 0
        while counter < index:
            node = node.next
            counter += 1

        return node

    def insert(self, index, item):
        if index < 0:
            index = 0
        elif index > len(self):
            index = len(self)

        if index == 0:
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(index - 1)
            cur_node.next = Node(item, cur_node.next)
        self.count += 1

    def append(self, item):
        if self.is_empty():
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(len(self) - 1)
            cur_node.next = Node (item, cur_node.next)
        self.count += 1







