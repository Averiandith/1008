# Factorial for loop and Factorial recursion


def factorial(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def factorial_recursive(n):
    if n == 0:
        return 1
    else:
        return n * factorial_recursive(n - 1)


print(factorial(5))
print("Factorial recursive,", factorial_recursive(5))


# Tail recursive version of factorial

def factorial_tail(n):
    return factorial_aux(n, 1)


def factorial_aux(n, result):
    if n == 0:
        return result
    else:
        return factorial_aux(n - 1, n * result)


print(factorial_tail(5))
print(factorial_aux(5, 1))


# Fibonacci for loop and recursion

def fibonacci(n):
    a = 0
    b = 1
    for i in range(0, n):
        previous = a
        a = b
        b = previous + b
    return a


def fibonacci_recursive(n):
    if n == 0 or n == 1:
        return n
    else:
        return fibonacci_recursive(n - 2) + fibonacci_recursive(n - 1)


print(fibonacci(5))
print(fibonacci_recursive(5))

# Fibonacci using tail recursion


def fibonacci_new(n):
    return fibonacci_aux(n, 0, 1)


def fibonacci_aux(n, before_last, last):
    if n == 0:
        return before_last
    else:
        return fibonacci_aux(n - 1, last, before_last + last)


print(fibonacci_new(5))
print(fibonacci_aux(5, 0, 1))

