class Node:
    def __init__(self, item, next):
        self.item = item
        self.next = next


class MyListIterator:
    def __init__(self, my_list):
        self.current = my_list.head

    def __iter__(self):
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration
        item = self.current.item
        self.current = self.current.next
        return item


class MyList:
    def __init__(self, size):
        assert size > 0, "size must be > 0"
        self.count = 0
        self.head = None

    def is_empty(self):
        return self.head is None

    def is_full(self):
        return False

    def append(self, item):
        if MyList is self.is_empty():
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(len(self)-1)
            cur_node.next = Node(item, cur_node.next)
        self.count += 1

    def insert(self, index, item):
        if index < 0:
            index = 0
        elif index > len(self):
            index = len(self)

        if index == 0:
            self.head = Node(item, self.head)
        else:
            cur_node = self.get_node(index - 1)
            cur_node.next = Node(item, cur_node.next)
        self.count += 1

    def __len2__(self):
        current = self.head
        counter = 0
        while current is not None:
            counter += 1
            current = current.next
        return counter

    def __len__(self):
        return self.__aux_len__(self.head)

    def __aux_len__(self, current):
        if current is None:
            return 0
        else:
            return 1 + self.__aux_len__(current.next)

    def __iter__(self):
        return MyListIterator(self)

    def get_node(self, index):
        if index < 0 or index >= len(self):
            raise IndexError("invalid index")
        node = self.head
        counter = 0
        while counter < index:
            node = node.next
            counter += 1
        return node

    def __contains2__(self, item):
        current = self.head
        while current is not None:
            if item == current.item:
                return True
            current = current.next
        return False

    def __contains__(self, item):
        return self.__aux_contains__(item, self.head)

    def __aux_contains__(self, item, current):
        if current is None:
            return False
        elif current.item == item:
            return True
        else:
            return self.__aux_contains__(item, current.next)

    # b_list = a_list.copy()

    def copy_iteration(self):
        current = self.head
        new_list = MyList(4)
        while current is not None:
            new_list.append(current.item)
            current = current.next
        return new_list

    def copy(self):
        new_list = MyList(4)
        return self.aux_copy(new_list, self.head)

    def aux_copy(self, new_list, current):
        if current is not None:
            return new_list
        else:
            self.aux_copy(new_list, current.next)
            new_list.insert(0, current.item)
            return new_list










