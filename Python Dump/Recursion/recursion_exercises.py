# Write a python program to calculate the sum of a list of numbers


def sum_a_list(a_list):
    result = 0
    for i in range(len(a_list)):
        result += a_list[i]
    return result


print(sum_a_list([1, 2, 3, 4, 5]))


def sum_a_list_recursive(a_list):
    if len(a_list) == 1:
        return a_list[0]
    else:
        return a_list[0] + sum_a_list_recursive(a_list[1:])


print(sum_a_list_recursive([1, 2, 3, 4, 5]))

# Testing list slicing
b_list = [1, 2, 3, 4, 5]
print(b_list[1:])

# Write a python program to convert an integer to a string in any base
