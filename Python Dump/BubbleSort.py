import random

"""def bubbleSort(theList):
    n = len(theList)
    i = 0
    while i < n - 1:
        j = 0
        while j < n - 1:
            if theList[j] > theList[j + 1]:
                theList[j], theList[j + 1] = theList[j + 1], theList[j]
            j += 1
        i += 1"""

def swap(aList, i, j):
    temp = aList[i]
    aList[i] = aList[j]
    aList[j] = temp

def bubbleSort(theList):
    n = len(theList)
    for i in range(n-1, 0 , -1):
        flag = True
        for j in range(i):
            if theList[j] > theList[j+1]:
                swap(theList, j, j+1)
                flag = False
        if flag:
            break



theList = [random.randint(1, 100) for i in range(20)]
bubbleSort(theList)
print(theList)


