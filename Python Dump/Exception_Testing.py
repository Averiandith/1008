x = [1, 2, 3]
while True:
    try:    # (Try) The testing phase
        i = int(input("Enter an index: ")) # Skip print if it is being passed into except
        print(x[i])     # Only execute if no exceptions
        break           # Exits
    except IndexError:  # (Except) The way to solve it
        print("Invalid index, please try again.")
    except ValueError:
        print("Input should be a number, please try again.")

print("Thank You \n")

def divide(a, b):
    """
    :param a: a must be a number
    :param b: b must be a number
    :return: if b is 0, ZeroDivisionError will be raised.
    """
    assert type(a) == int, "a must be a number"  # Treat assert as a form of precondition
    assert type(b) == int, "b must be a number"
    return a // b

def divideUnitTest():
    assert divide(4, 2) == 2, "test failed"
    assert divide(9, -3) == -3, "test failed"
    try:
        divide(3, 0)
        print("test failed")
    except ZeroDivisionError:
        pass
    print("Test Passed")

divideUnitTest()

def getHeight():
    h = int(input("Please enter your height: "))
    if h < 0:
        myError = ValueError("User gave invalid height")
        raise myError
    return("Passed", h)

getHeight()

